# djiango-sueldosbeneficios

sistema interno de la direccion de sueldos y beneficios el mec

## Configuración del Entorno Virtual


**Crear y Activar el Entorno Virtual:**
   ```bash   
   python -m venv venv
   source venv/bin/activate  # Para sistemas basados en Unix (Linux/Mac)
   # o
   .\venv\Scripts\activate  # Para sistemas basados en Windows
```


**Instalar dependencias**
   ```bash
   pip install -r requirements.txt
```



**Ejecutar en servidor local**
   ```bash
    python manage.py runserver
```





**Ejecutar en servidor local**
   ```bash
    python manage.py runserver
```




## usar git colaborativo


#### Configuración Inicial (Solo una vez por usuario):

**Clonar el Repositorio:**
   ```bash
    git clone https://gitlab.com/hugomrj/sueldosbeneficios.git
    cd sueldosbeneficios
```




#### Flujo de Trabajo Diario (Para Cada Sesión de Trabajo):


**Actualizar el Repositorio Local:**
   ```bash
   git pull origin main
```
Esto asegura que tu rama main local esté sincronizada con la rama main remota.



**Hacer Cambios Locales:**
   ```bash
    # Realizar cambios en tu código
    git add .
    git commit -m "Mensaje descriptivo de los cambios"
```
Esto confirma tus cambios localmente.





**Obtener Cambios Remotos y Fusionarlos (Si es necesario):**
   ```bash
   git pull origin main
```
Si hay cambios en la rama main remota, este paso ayuda a fusionar esos cambios con tus cambios locales. Si hay conflictos, Git te lo notificará.




**Enviar Cambios al Repositorio Remoto (main):**
   ```bash    
   git push --set-upstream origin main
```
Esto envía tus cambios al repositorio remoto.



