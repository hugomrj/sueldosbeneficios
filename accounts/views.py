from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views import View




class Login (View):    
    template_name = 'accounts/login.html'

    def get(self, request, *args, **kwargs):
        # Cerrar sesión antes de mostrar la página de inicio de sesión
        logout(request)        
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        
        # Cerrar sesión antes de intentar iniciar sesión nuevamente
        logout(request)        
        
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        
        if user is not None:                        
            login(request, user)
            return redirect('home')
        else:
            messages.error(request, 'Credenciales inválidas. Inténtalo de nuevo.')

        # return render(request, self.template_name)
        # Retornar la respuesta para redirigir a la página de inicio de sesión
        return redirect('login')        





class Home (View, LoginRequiredMixin):
    template_name = 'accounts/home.html'

    def get(self, request, *args, **kwargs):


        # Verifica el rol desde el middleware
        es_sysadmin = request.role == 'SysAdmin'
        es_aplicacion = request.role == 'Aplicacion'

        context = {
            'es_sysadmin': es_sysadmin,
            'es_aplicacion': es_aplicacion,
        }

        return render(request, self.template_name, context)

