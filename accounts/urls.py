from django.urls import path

from accounts.views import Home, Login


urlpatterns = [    
    path('', Login.as_view() , name='login'),                          
    path('login/', Login.as_view() , name='login'),
    path('home/', Home.as_view(), name='home'),
]    



