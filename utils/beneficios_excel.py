import re
import pandas as pd
from utils.conexion_db import establecer_conexion
from datetime import datetime


def procesar_beneficios_excel(archivo_excel, anio, mes):
    try:        

        # Crear un objeto ArgumentParser
        # parser = argparse.ArgumentParser()
        # parser.add_argument('--anio', type=int, help='Año:', required=True)
        # parser.add_argument('--mes', type=int, help='Mes:', required=True)
        # args = parser.parse_args()

        # para acceder a los valores de anio y mes a través de args.anio y args.mes
        # anio_filtro = args.anio
        # mes_filtro = args.mes

        anio_filtro = anio
        mes_filtro = mes


        # Establece la conexión a la base de datos
        connection = establecer_conexion()


        # Crear un cursor para ejecutar consultas SQL
        cursor = connection.cursor()


        # Eliminar registros de la tabla marcacion_detalle    
        cursor.execute("DELETE FROM marcacion_detalle WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        # Eliminar registros de la tabla marcacion_resumen    
        # cursor.execute("DELETE FROM marcacion_resumen WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        # Eliminar registros de la tabla marcacion_ubicacion
        # cursor.execute("DELETE FROM marcacion_ubicacion WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        


        # Ruta al archivo de Excel
        #archivo_excel = r'C:\Users\Usuario\Documents\Sueldos y Beneficios\RERA\crispin\solicitudes_remuneracion_adicional_202310.xlsx'


        # Leer el archivo de Excel en un DataFrame de pandas
        # df_detalle  = pd.read_excel(archivo_excel, sheet_name='DetalleGeneral')
        # df_detalle  = pd.read_excel(archivo_excel, sheet_name='Detalle')
        df_detalle = pd.read_excel(archivo_excel)
        



        # Iterar a través de las filas del DataFrame y insertar en la tabla MySQL
        for _, row in df_detalle.iterrows():
                
            # Definir valores a insertar
            codigo_trabajador = row['codigo_trabajador']
            codigo_trabajador = re.sub(r'\D', '', str(codigo_trabajador))
            
            nombre_trabajador = row['nombre_trabajador']
            codigo_dependencia = row['codigo_dependencia']
            nombre_dependencia = row['nombre_dependencia']
            codigo_dependencia_superior = row['codigo_dependencia_superior']
            nombre_dependencia_superior = row['nombre_dependencia_superior']
            codigo_cargo = row['codigo_cargo']
            nombre_cargo = row['nombre_cargo']
            cantidad_horas_remuneracion_extraordinaria = row['cantidad_horas_remuneracion_extraordinaria']
            cantidad_horas_remuneracion_adicional = row['cantidad_horas_remuneracion_adicional']
            
            codigo_categoria_rubro = row['codigo_categoria_rubro']
            if pd.isna(codigo_categoria_rubro) or codigo_categoria_rubro.lower() == 'null':
                codigo_categoria_rubro = None

            monto_categoria_rubro = row['monto_categoria_rubro']
            if pd.isna(monto_categoria_rubro) or str(monto_categoria_rubro).lower() == 'null':    
                monto_categoria_rubro = None
            
            codigo_tipo_categoria_rubro = row['codigo_tipo_categoria_rubro']
            if pd.isna(codigo_tipo_categoria_rubro) or codigo_tipo_categoria_rubro.lower() == 'null':
                codigo_tipo_categoria_rubro = None

            cantidad_rubro = row['cantidad_rubro']
            if pd.isna(cantidad_rubro) or str(cantidad_rubro).lower() == 'null':    
                cantidad_rubro = None
            
            total_asignacion = row['total_asignacion']
            if pd.isna(total_asignacion) or str(total_asignacion).lower() == 'null':    
                total_asignacion = None

            codigo_turno = row['codigo_turno']
            if pd.isna(codigo_turno) or codigo_turno.lower() == 'null':
                codigo_turno = None
            
            codigo_periodo_excepcion_asistencia = row['codigo_periodo_excepcion_asistencia']
                    
                
            """
            total_horas_aplicada_re = row['total_horas_aplicada_re']
            if pd.isna(total_horas_aplicada_re) or str(total_horas_aplicada_re).lower() == 'null':
                total_horas_aplicada_re = None
            else:        
                total_horas_aplicada_re = round(float(total_horas_aplicada_re), 2)
            """


            total_horas_aplicada_re = row['total_horas_aplicada_re']
            if pd.isna(total_horas_aplicada_re) or str(total_horas_aplicada_re).lower() == 'null':
                total_horas_aplicada_re = None
            else:
                # Reemplaza la coma por un punto y luego convierte a float
                total_horas_aplicada_re = round(float(str(total_horas_aplicada_re).replace(',', '.')), 2)



            total_horas_50_ra = row['total_horas_50_ra']
            if pd.isna(total_horas_50_ra) or str(total_horas_50_ra).lower() == 'null':
                total_horas_50_ra = None
            else:                    
                total_horas_50_ra = round(float(str(total_horas_50_ra).replace(',', '.')), 2)


            total_horas_100_ra = row['total_horas_100_ra']
            if pd.isna(total_horas_100_ra) or str(total_horas_100_ra).lower() == 'null':
                total_horas_100_ra = None
            else:                    
                total_horas_100_ra = round(float(str(total_horas_100_ra).replace(',', '.')), 2)


            total_horas_aplicada_ra = row['total_horas_aplicada_ra']
            if pd.isna(total_horas_aplicada_ra) or str(total_horas_aplicada_ra).lower() == 'null':
                total_horas_aplicada_ra = None
            else:                    
                total_horas_aplicada_ra = round(float(str(total_horas_aplicada_ra).replace(',', '.')), 2)

            
            numero_tipo_condicion_laboral = row['numero_tipo_condicion_laboral']
            codigo_tipo_condicion_laboral = row['codigo_tipo_condicion_laboral']
            

            # Consulta SQL para insertar un registro en la tabla marcacion_detalle
            # insert_query = "INSERT INTO marcacion_detalle (codigo_trabajador, nombre_trabajador, codigo_dependencia) VALUES (%s, %s, %s)"
            
            # Crear la cadena SQL como una f-string
            insert_query = f"""
            INSERT INTO marcacion_detalle
                (codigo_trabajador,  nombre_trabajador, codigo_dependencia,
                nombre_dependencia,  codigo_dependencia_superior, nombre_dependencia_superior,
                codigo_cargo,  nombre_cargo, cantidad_horas_remuneracion_extraordinaria,
                cantidad_horas_remuneracion_adicional, codigo_categoria_rubro, monto_categoria_rubro,
                codigo_tipo_categoria_rubro, cantidad_rubro, total_asignacion, codigo_turno,
                codigo_periodo_excepcion_asistencia, total_horas_aplicada_re, total_horas_50_ra,
                total_horas_100_ra, total_horas_aplicada_ra, numero_tipo_condicion_laboral,
                codigo_tipo_condicion_laboral, anio, mes)
            VALUES
                ( %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s
                );
            """
            
            data = (
                codigo_trabajador, nombre_trabajador, codigo_dependencia, nombre_dependencia,
                codigo_dependencia_superior, nombre_dependencia_superior, codigo_cargo, nombre_cargo,
                cantidad_horas_remuneracion_extraordinaria, cantidad_horas_remuneracion_adicional,
                codigo_categoria_rubro, monto_categoria_rubro, codigo_tipo_categoria_rubro, cantidad_rubro,
                total_asignacion, codigo_turno, codigo_periodo_excepcion_asistencia, total_horas_aplicada_re,
                total_horas_50_ra,  total_horas_100_ra, total_horas_aplicada_ra, numero_tipo_condicion_laboral,
                codigo_tipo_condicion_laboral, anio_filtro, mes_filtro
                    )

            # Imprime la consulta SQL con datos insertados
            # print(insert_query % data)

            cursor.execute(insert_query, data)

        # Confirmar los cambios en la base de datos
        connection.commit()


        # Informar que la inserción se ha completado
        print("Registros insertados hoja marcacion")


        # Cerrar el cursor y la conexión
        cursor.close()
        connection.close()



        return True

    except Exception as e:
        # Manejar cualquier excepción y devolver False indicando error
        print(f"Error: {str(e)}")
        return False
