import re
import pandas as pd
from utils.conexion_db import establecer_conexion
from datetime import datetime



def migrar_contratados_excel(archivo_excel, anio, mes):
    try:
        anio_filtro = anio
        mes_filtro = mes

        # Establecer la conexión a la base de datos
        connection = establecer_conexion()

        # Crear un cursor para ejecutar consultas SQL
        cursor = connection.cursor()

        # Eliminar registros de la tabla
        cursor.execute("DELETE FROM contratados WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        # Leer el archivo de Excel en un DataFrame de pandas
        hoja1 = pd.read_excel(archivo_excel, sheet_name='Hoja1')

        # Iterar a través de las filas del DataFrame e insertar en la tabla MySQL
        for _, row in hoja1.iterrows():
            objeto = row['objeto']
            cedula = row['cedula']
            
            nombre = row['nombre']
            if pd.isna(nombre) or nombre.lower() == 'null':
                nombre = None
                
            id_depende = row['id_depende']                
                         
            dependencia = row['dependencia']
            if pd.isna(dependencia) or dependencia.lower() == 'null':
                dependencia = None
            
            nro_contrato = row['nro_contrato']    
            presupuesto = row['presupuesto'] 
            devengado = row['devengado'] 
            iva_30 = row['iva_30'] 
            iva_10 = row['iva_10'] 
            multa = row['multa'] 
            judiciales = row['judiciales'] 
            liquido = row['liquido'] 
            
            contribuyente = row['contribuyente'] 
            tiene_judicial = row['tiene_judicial'] 
            
            id_estado_pago = row['id_estado_pago'] 
            
            gestion = row['gestion'] 
            if pd.isna(gestion) or gestion.lower() == 'null':
                gestion = None                         
                         
            nro_planilla = row['nro_planilla']                         
                         
                               

            # Crear la cadena SQL como una f-string
            insert_query = f"""
                INSERT INTO contratados
                    (objeto, cedula, nombre, id_depende, dependencia,
                    nro_contrato, presupuesto, devengado, iva_30, iva_10,
                    multa, judiciales, liquido, contribuyente, tiene_judicial, 
                    id_estado_pago, gestion, nro_planilla, 
                    anio, mes)
                VALUES
                    (%s, %s, %s, %s, %s,
                    %s, %s, %s, %s, %s,
                    %s, %s, %s, %s, %s, 
                    %s, %s, %s,
                    %s, %s);
            """
        
            data = (objeto, cedula, nombre, id_depende, dependencia,
                    nro_contrato, presupuesto, devengado, iva_30, iva_10,
                    multa, judiciales, liquido, contribuyente, tiene_judicial,
                    id_estado_pago, gestion, nro_planilla,
                    anio_filtro, mes_filtro)

            cursor.execute(insert_query, data)

        # Confirmar los cambios en la base de datos
        connection.commit()

        # Cerrar el cursor y la conexión
        cursor.close()
        connection.close()

        # Informar que la inserción se ha completado
        print("Registros insertados")

        # Devolver True indicando éxito
        return True
    
    except Exception as e:
        # Manejar cualquier excepción y devolver False indicando error
        print(f"Error: {str(e)}")
        return False
