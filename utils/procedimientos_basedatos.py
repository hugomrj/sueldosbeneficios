from django.db import connection


# procedimiento almacenado en mysql que calcula el RE

def ejecutar_procedimiento_almacenado_re(anio, mes):


    print('entra aca en ejecutar_procedimiento_almacenado_re ')

    try:
        with connection.cursor() as cursor:
            cursor.execute('CALL Generar_Liquidacion_RE(%s, %s)', [anio, mes])
        # Si la ejecución del procedimiento almacenado no lanzó excepciones, se considera exitoso
        return True
    
    except Exception as e:
        # En caso de una excepción, puedes imprimir el mensaje de error si lo necesitas
        print(f"Error al ejecutar el procedimiento almacenado: {str(e)}")
        # Puedes retornar False u otra indicación de que hubo un problema
        return False