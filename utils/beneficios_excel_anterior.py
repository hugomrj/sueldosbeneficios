import re
import pandas as pd
from utils.conexion_db import establecer_conexion
from datetime import datetime


def procesar_beneficios_excel(archivo_excel, anio, mes):
    try:        

        # Crear un objeto ArgumentParser
        # parser = argparse.ArgumentParser()
        # parser.add_argument('--anio', type=int, help='Año:', required=True)
        # parser.add_argument('--mes', type=int, help='Mes:', required=True)
        # args = parser.parse_args()

        # para acceder a los valores de anio y mes a través de args.anio y args.mes
        # anio_filtro = args.anio
        # mes_filtro = args.mes

        anio_filtro = anio
        mes_filtro = mes


        # Establece la conexión a la base de datos
        connection = establecer_conexion()


        # Crear un cursor para ejecutar consultas SQL
        cursor = connection.cursor()


        # Eliminar registros de la tabla marcacion_detalle    
        cursor.execute("DELETE FROM marcacion_detalle WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        # Eliminar registros de la tabla marcacion_resumen    
        cursor.execute("DELETE FROM marcacion_resumen WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        # Eliminar registros de la tabla marcacion_ubicacion
        cursor.execute("DELETE FROM marcacion_ubicacion WHERE anio = %s AND mes = %s", [anio_filtro, mes_filtro])

        


        # Ruta al archivo de Excel
        #archivo_excel = r'C:\Users\Usuario\Documents\Sueldos y Beneficios\RERA\crispin\solicitudes_remuneracion_adicional_202310.xlsx'


        # Leer el archivo de Excel en un DataFrame de pandas
        # df_detalle  = pd.read_excel(archivo_excel, sheet_name='DetalleGeneral')
        df_detalle  = pd.read_excel(archivo_excel, sheet_name='Detalle')
        df_resumen = pd.read_excel(archivo_excel, sheet_name='ResumenPorTrabajador')
        df_ubicacion = pd.read_excel(archivo_excel, sheet_name='UbicacionLaboral')



        # Iterar a través de las filas del DataFrame y insertar en la tabla MySQL
        for _, row in df_detalle.iterrows():
                
            # Definir valores a insertar
            codigo_trabajador = row['codigo_trabajador']
            codigo_trabajador = re.sub(r'\D', '', str(codigo_trabajador))
            
            nombre_trabajador = row['nombre_trabajador']
            codigo_dependencia = row['codigo_dependencia']
            nombre_dependencia = row['nombre_dependencia']
            codigo_dependencia_superior = row['codigo_dependencia_superior']
            nombre_dependencia_superior = row['nombre_dependencia_superior']
            codigo_cargo = row['codigo_cargo']
            nombre_cargo = row['nombre_cargo']
            cantidad_horas_remuneracion_extraordinaria = row['cantidad_horas_remuneracion_extraordinaria']
            cantidad_horas_remuneracion_adicional = row['cantidad_horas_remuneracion_adicional']
            
            codigo_categoria_rubro = row['codigo_categoria_rubro']
            if pd.isna(codigo_categoria_rubro) or codigo_categoria_rubro.lower() == 'null':
                codigo_categoria_rubro = None

            monto_categoria_rubro = row['monto_categoria_rubro']
            if pd.isna(monto_categoria_rubro) or str(monto_categoria_rubro).lower() == 'null':    
                monto_categoria_rubro = None
            
            codigo_tipo_categoria_rubro = row['codigo_tipo_categoria_rubro']
            if pd.isna(codigo_tipo_categoria_rubro) or codigo_tipo_categoria_rubro.lower() == 'null':
                codigo_tipo_categoria_rubro = None

            cantidad_rubro = row['cantidad_rubro']
            if pd.isna(cantidad_rubro) or str(cantidad_rubro).lower() == 'null':    
                cantidad_rubro = None
            
            total_asignacion = row['total_asignacion']
            if pd.isna(total_asignacion) or str(total_asignacion).lower() == 'null':    
                total_asignacion = None

            codigo_turno = row['codigo_turno']
            if pd.isna(codigo_turno) or codigo_turno.lower() == 'null':
                codigo_turno = None
            
            codigo_periodo_excepcion_asistencia = row['codigo_periodo_excepcion_asistencia']
                    
                
            """
            total_horas_aplicada_re = row['total_horas_aplicada_re']
            if pd.isna(total_horas_aplicada_re) or str(total_horas_aplicada_re).lower() == 'null':
                total_horas_aplicada_re = None
            else:        
                total_horas_aplicada_re = round(float(total_horas_aplicada_re), 2)
            """


            total_horas_aplicada_re = row['total_horas_aplicada_re']
            if pd.isna(total_horas_aplicada_re) or str(total_horas_aplicada_re).lower() == 'null':
                total_horas_aplicada_re = None
            else:
                # Reemplaza la coma por un punto y luego convierte a float
                total_horas_aplicada_re = round(float(str(total_horas_aplicada_re).replace(',', '.')), 2)



            total_horas_50_ra = row['total_horas_50_ra']
            if pd.isna(total_horas_50_ra) or str(total_horas_50_ra).lower() == 'null':
                total_horas_50_ra = None
            else:                    
                total_horas_50_ra = round(float(str(total_horas_50_ra).replace(',', '.')), 2)


            total_horas_100_ra = row['total_horas_100_ra']
            if pd.isna(total_horas_100_ra) or str(total_horas_100_ra).lower() == 'null':
                total_horas_100_ra = None
            else:                    
                total_horas_100_ra = round(float(str(total_horas_100_ra).replace(',', '.')), 2)


            total_horas_aplicada_ra = row['total_horas_aplicada_ra']
            if pd.isna(total_horas_aplicada_ra) or str(total_horas_aplicada_ra).lower() == 'null':
                total_horas_aplicada_ra = None
            else:                    
                total_horas_aplicada_ra = round(float(str(total_horas_aplicada_ra).replace(',', '.')), 2)

            
            numero_tipo_condicion_laboral = row['numero_tipo_condicion_laboral']
            codigo_tipo_condicion_laboral = row['codigo_tipo_condicion_laboral']
            

            # Consulta SQL para insertar un registro en la tabla marcacion_detalle
            # insert_query = "INSERT INTO marcacion_detalle (codigo_trabajador, nombre_trabajador, codigo_dependencia) VALUES (%s, %s, %s)"
            
            # Crear la cadena SQL como una f-string
            insert_query = f"""
            INSERT INTO marcacion_detalle
                (codigo_trabajador,  nombre_trabajador, codigo_dependencia,
                nombre_dependencia,  codigo_dependencia_superior, nombre_dependencia_superior,
                codigo_cargo,  nombre_cargo, cantidad_horas_remuneracion_extraordinaria,
                cantidad_horas_remuneracion_adicional, codigo_categoria_rubro, monto_categoria_rubro,
                codigo_tipo_categoria_rubro, cantidad_rubro, total_asignacion, codigo_turno,
                codigo_periodo_excepcion_asistencia, total_horas_aplicada_re, total_horas_50_ra,
                total_horas_100_ra, total_horas_aplicada_ra, numero_tipo_condicion_laboral,
                codigo_tipo_condicion_laboral, anio, mes)
            VALUES
                ( %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s
                );
            """
            
            data = (
                codigo_trabajador, nombre_trabajador, codigo_dependencia, nombre_dependencia,
                codigo_dependencia_superior, nombre_dependencia_superior, codigo_cargo, nombre_cargo,
                cantidad_horas_remuneracion_extraordinaria, cantidad_horas_remuneracion_adicional,
                codigo_categoria_rubro, monto_categoria_rubro, codigo_tipo_categoria_rubro, cantidad_rubro,
                total_asignacion, codigo_turno, codigo_periodo_excepcion_asistencia, total_horas_aplicada_re,
                total_horas_50_ra,  total_horas_100_ra, total_horas_aplicada_ra, numero_tipo_condicion_laboral,
                codigo_tipo_condicion_laboral, anio_filtro, mes_filtro
                    )

            # Imprime la consulta SQL con datos insertados
            # print(insert_query % data)

            cursor.execute(insert_query, data)

        # Confirmar los cambios en la base de datos
        connection.commit()


        # Informar que la inserción se ha completado
        print("Registros insertados 1ra hoja")


        # segunda hoja resumen
        for _, row in df_resumen.iterrows():
                
            # Definir valores a insertar
            codigo_trabajador = row['codigo_trabajador']
            codigo_trabajador = re.sub(r'\D', '', str(codigo_trabajador))
            
            nombre_trabajador = row['nombre_trabajador']
            codigo_dependencia = row['codigo_dependencia']
            nombre_dependencia = row['nombre_dependencia']
            codigo_dependencia_superior = row['codigo_dependencia_superior']
            nombre_dependencia_superior = row['nombre_dependencia_superior']
            codigo_cargo = row['codigo_cargo']
            nombre_cargo = row['nombre_cargo']
            cantidad_horas_remuneracion_extraordinaria = row['cantidad_horas_remuneracion_extraordinaria']
            cantidad_horas_remuneracion_adicional = row['cantidad_horas_remuneracion_adicional']
        
            
            codigo_periodo_excepcion_asistencia = row['codigo_periodo_excepcion_asistencia']
            
            total_horas_aplicada_re = row['total_horas_aplicada_re']    
            if pd.isna(total_horas_aplicada_re) or str(total_horas_aplicada_re).lower() == 'null':    
                total_horas_aplicada_re = None    
            
            total_horas_50_ra = row['total_horas_50_ra']
            if pd.isna(total_horas_50_ra) or str(total_horas_50_ra).lower() == 'null':    
                total_horas_50_ra = None    

            total_horas_100_ra = row['total_horas_100_ra']
            if pd.isna(total_horas_100_ra) or str(total_horas_100_ra).lower() == 'null':    
                total_horas_100_ra = None    
            
            total_horas_aplicada_ra = row['total_horas_aplicada_ra']
            if pd.isna(total_horas_aplicada_ra) or str(total_horas_aplicada_ra).lower() == 'null':    
                total_horas_aplicada_ra = None        
                

            # Crear la cadena SQL como una f-string
            insert_query = f"""
            INSERT INTO marcacion_resumen
                (codigo_trabajador,  nombre_trabajador, codigo_dependencia,
                nombre_dependencia,  codigo_dependencia_superior, nombre_dependencia_superior,
                codigo_cargo,  nombre_cargo, cantidad_horas_remuneracion_extraordinaria,
                cantidad_horas_remuneracion_adicional,          
                codigo_periodo_excepcion_asistencia, total_horas_aplicada_re, total_horas_50_ra,
                total_horas_100_ra, total_horas_aplicada_ra, anio, mes
                )
            VALUES
                ( %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, 
                %s, %s, %s, %s, %s, %s, %s 
                );
            """
            
            data = (
                codigo_trabajador, nombre_trabajador, codigo_dependencia, nombre_dependencia,
                codigo_dependencia_superior, nombre_dependencia_superior, codigo_cargo, nombre_cargo,
                cantidad_horas_remuneracion_extraordinaria, cantidad_horas_remuneracion_adicional,        
                codigo_periodo_excepcion_asistencia, total_horas_aplicada_re,
                total_horas_50_ra,  total_horas_100_ra, total_horas_aplicada_ra, anio_filtro, mes_filtro       
                )

            # Imprime la consulta SQL con datos insertados
            # print(insert_query % data)
            
            cursor.execute(insert_query, data)

        # Confirmar los cambios en la base de datos
        connection.commit()

        # Informar que la inserción se ha completado
        print("Registros insertados 2da hoja")






        # tercera hoja resumen
        for _, row in df_ubicacion.iterrows():
                
                
                
            # Definir valores a insertar
            codigo_puesto = row['codigo_puesto']
            codigo_dependencia = row['codigo_dependencia']
            nombre_dependencia = row['nombre_dependencia']
            id_jurisdiccion = row['id_jurisdiccion']        
            codigo_jurisdiccion_02 = str(row['codigo_jurisdiccion_02']).zfill(2)
            
            nombre_jurisdiccion_02 = row['nombre_jurisdiccion_02']            
            if pd.isna(nombre_jurisdiccion_02) or nombre_jurisdiccion_02.lower() == 'null':
                nombre_jurisdiccion_02 = None
            
            nombre_jurisdiccion_03 = row['nombre_jurisdiccion_03']            
            if pd.isna(nombre_jurisdiccion_03) or nombre_jurisdiccion_03.lower() == 'null':
                nombre_jurisdiccion_03 = None    

            nombre_jurisdiccion_04 = row['nombre_jurisdiccion_04']            
            if pd.isna(nombre_jurisdiccion_04) or nombre_jurisdiccion_04.lower() == 'null':
                nombre_jurisdiccion_04 = None    
            
            codigo_institucion = row['codigo_institucion']    
            if pd.isna(codigo_institucion) or str(codigo_institucion).lower() == 'null':    
                codigo_institucion = None  
            
            nombre_institucion = row['nombre_institucion']            
            if pd.isna(nombre_institucion) or nombre_institucion.lower() == 'null':
                nombre_institucion = None    
            
            codigo_nivel_educativo = row['codigo_nivel_educativo']    
            if pd.isna(codigo_nivel_educativo) or str(codigo_nivel_educativo).lower() == 'null':    
                codigo_nivel_educativo = None         
            

            codigo_trabajador = row['codigo_trabajador']
            codigo_trabajador = re.sub(r'\D', '', str(codigo_trabajador))


            letra_persona = row['letra_persona']            
            if pd.isna(letra_persona) or letra_persona.lower() == 'null':
                letra_persona = None      
                    
            fecha_nacimiento = row['fecha_nacimiento']            
            fecha_nacimiento = datetime.strptime(fecha_nacimiento, '%d/%m/%Y').strftime('%Y-%m-%d')
            
            direccion_persona = row['direccion_persona']            
            if pd.isna(direccion_persona) or direccion_persona.lower() == 'null':
                direccion_persona = None   
            
            correo_electronico_persona = row['correo_electronico_persona']            
            if pd.isna(correo_electronico_persona) or correo_electronico_persona.lower() == 'null':
                correo_electronico_persona = None   
            
            nombre_trabajador = row['nombre_trabajador']            
            if pd.isna(nombre_trabajador) or nombre_trabajador.lower() == 'null':
                nombre_trabajador = None       
            
            fecha_ingreso = row['fecha_ingreso']            
            fecha_ingreso = datetime.strptime(fecha_ingreso, '%d/%m/%Y').strftime('%Y-%m-%d')
            
            antig_anho = row['antig_anho']  
            antig_mes = row['antig_mes']  
            numero_turno = row['numero_turno']  
            codigo_turno = row['codigo_turno']  
            nombre_cargo = row['nombre_cargo']  
                
            id_seccion = row['id_seccion']    
            if pd.isna(id_seccion) or str(id_seccion).lower() == 'null':    
                id_seccion = None      

            letra_numero_seccion = row['letra_numero_seccion']            
            if pd.isna(letra_numero_seccion) or letra_numero_seccion.lower() == 'null':
                letra_numero_seccion = None   

            codigo_seccion = row['codigo_seccion']            
            if pd.isna(codigo_seccion) or codigo_seccion.lower() == 'null':
                codigo_seccion = None   

            cantidad_alumnos = row['cantidad_alumnos']                           
            if pd.isna(cantidad_alumnos)  :
                cantidad_alumnos = None       

            numero_curso = row['numero_curso']                           
            if pd.isna(numero_curso)  :
                numero_curso = None       
            
            codigo_curso = row['codigo_curso']                           
            if pd.isna(codigo_curso)  :
                codigo_curso = None       
            
            id_curriculo = row['id_curriculo']                           
            if pd.isna(id_curriculo)  :
                id_curriculo = None       
            
            nombre_curriculo = row['nombre_curriculo']                           
            if pd.isna(nombre_curriculo)  :
                nombre_curriculo = None       
            
            descripcion_curriculo = row['descripcion_curriculo']                           
            if pd.isna(descripcion_curriculo)  :
                descripcion_curriculo = None       
            
            codigo_categoria_rubro = row['codigo_categoria_rubro']                           
            if pd.isna(codigo_categoria_rubro)  :
                codigo_categoria_rubro = None       
            
            monto_categoria_rubro = row['monto_categoria_rubro']                           
            if pd.isna(monto_categoria_rubro)  :
                monto_categoria_rubro = None       
            
            horas_catedras = row['horas_catedras']                           
            if pd.isna(horas_catedras)  :
                horas_catedras = None       
            
            cantidad_concepto_puesto = row['cantidad_concepto_puesto']                           
            if pd.isna(cantidad_concepto_puesto)  :
                cantidad_concepto_puesto = None       
            
            asignacion = row['asignacion']                           
            if pd.isna(asignacion)  :
                asignacion = None       
            
            titulo_persona = row['titulo_persona']                           
            if pd.isna(titulo_persona)  :
                titulo_persona = None   
                
            codigo_tipo_categoria_rubro = row['codigo_tipo_categoria_rubro']                           
            if pd.isna(codigo_tipo_categoria_rubro)  :
                codigo_tipo_categoria_rubro = None   
                
            numero_tipo_categoria_rubro = row['numero_tipo_categoria_rubro']                           
            if pd.isna(numero_tipo_categoria_rubro)  :
                numero_tipo_categoria_rubro = None   
                
            mes_asig_rubro = row['mes_asig_rubro']                           
            if pd.isna(mes_asig_rubro)  :
                mes_asig_rubro = None   
                
            anio_asig_rubro = row['anio_asig_rubro']                           
            if pd.isna(anio_asig_rubro)  :
                anio_asig_rubro = None   
                
            numero_matriculacion = row['numero_matriculacion']                           
            if pd.isna(numero_matriculacion)  :
                numero_matriculacion = None   
                
            carga_horaria_mensual = row['carga_horaria_mensual']                           
            if pd.isna(carga_horaria_mensual)  :
                carga_horaria_mensual = None   
                
            carga_horaria_mensual = row['carga_horaria_mensual']                           
            if pd.isna(carga_horaria_mensual)  :
                carga_horaria_mensual = None   
                
            nombre_disciplina = row['nombre_disciplina']                           
            if pd.isna(nombre_disciplina)  :
                nombre_disciplina = None   
                
            numero_persona = row['numero_persona']                           
            if pd.isna(numero_persona)  :
                numero_persona = None   
                
            numero_tipo_cargo = row['numero_tipo_cargo']                           
            if pd.isna(numero_tipo_cargo)  :
                numero_tipo_cargo = None   
                
            numero_tipo_gestion = row['numero_tipo_gestion']                           
            if pd.isna(numero_tipo_gestion)  :
                numero_tipo_gestion = None   
                
            numero_nivel_educativo = row['numero_nivel_educativo']                           
            if pd.isna(numero_nivel_educativo)  :
                numero_nivel_educativo = None   
                
            id_puesto = row['id_puesto']                           
            if pd.isna(id_puesto)  :
                id_puesto = None   
                
            es_categoria_administrativa = row['es_categoria_administrativa']                           
            if pd.isna(es_categoria_administrativa)  :
                es_categoria_administrativa = None   
                
            codigo_tipo_cargo = row['codigo_tipo_cargo']                           
            if pd.isna(codigo_tipo_cargo)  :
                codigo_tipo_cargo = None   
                
            es_administracion_central = row['es_administracion_central']                           
            if pd.isna(es_administracion_central)  :
                es_administracion_central = None   
                
            codigo_cargo = row['codigo_cargo']                           
            if pd.isna(codigo_cargo)  :
                codigo_cargo = None   
                
            numero_tipo_administracion = row['numero_tipo_administracion']                           
            if pd.isna(numero_tipo_administracion)  :
                numero_tipo_administracion = None   
                
            codigo_tipo_administracion = row['codigo_tipo_administracion']                           
            if pd.isna(codigo_tipo_administracion)  :
                codigo_tipo_administracion = None   
                
            equivalencia_hora_turno = row['equivalencia_hora_turno']                           
            if pd.isna(equivalencia_hora_turno)  :
                equivalencia_hora_turno = None   
                
            codigo_tipo_gestion = row['codigo_tipo_gestion']                           
            if pd.isna(codigo_tipo_gestion)  :
                codigo_tipo_gestion = None   
                
            codigo_tipo_zona = row['codigo_tipo_zona']                           
            if pd.isna(codigo_tipo_zona)  :
                codigo_tipo_zona = None   
                
            direccion_dependencia = row['direccion_dependencia']                           
            if pd.isna(direccion_dependencia)  :
                direccion_dependencia = None   
                
            telefono_dependencia = row['telefono_dependencia']                           
            if pd.isna(telefono_dependencia)  :
                telefono_dependencia = None   
                
            numero_tipo_institucion = row['numero_tipo_institucion']                           
            if pd.isna(numero_tipo_institucion)  :
                numero_tipo_institucion = None   
                
            codigo_tipo_institucion = row['codigo_tipo_institucion']                           
            if pd.isna(codigo_tipo_institucion)  :
                codigo_tipo_institucion = None   
                
            codigo_tipo_presupuesto_puesto = row['codigo_tipo_presupuesto_puesto']                           
            if pd.isna(codigo_tipo_presupuesto_puesto)  :
                codigo_tipo_presupuesto_puesto = None   
                
            codigo_dependencia_apn = row['codigo_dependencia_apn']                           
            if pd.isna(codigo_dependencia_apn)  :
                codigo_dependencia_apn = None   
                
            nombre_dependencia_apn = row['nombre_dependencia_apn']                           
            if pd.isna(nombre_dependencia_apn)  :
                nombre_dependencia_apn = None   
                
            numero_tipo_dependencia = row['numero_tipo_dependencia']                           
            if pd.isna(numero_tipo_dependencia)  :
                numero_tipo_dependencia = None   
                
            codigo_tipo_dependencia = row['codigo_tipo_dependencia']                           
            if pd.isna(codigo_tipo_dependencia)  :
                codigo_tipo_dependencia = None   
                
            es_ad_honorem = row['es_ad_honorem']                           
            if pd.isna(es_ad_honorem)  :
                es_ad_honorem = None   
                
            es_voluntario = row['es_voluntario']                           
            if pd.isna(es_voluntario)  :
                es_voluntario = None   
                
            anio_ad_honorem = row['anio_ad_honorem']                           
            if pd.isna(anio_ad_honorem)  :
                anio_ad_honorem = None   
                
            mes_ad_honorem = row['mes_ad_honorem']                           
            if pd.isna(mes_ad_honorem)  :
                mes_ad_honorem = None   
                

            fecha_puesto_efectivo_verificado = row['fecha_puesto_efectivo_verificado']
            if not isinstance(fecha_puesto_efectivo_verificado, str) or not fecha_puesto_efectivo_verificado.strip():  
                fecha_puesto_efectivo_verificado = None
            else:
                fecha_puesto_efectivo_verificado = datetime.strptime(fecha_puesto_efectivo_verificado, '%d/%m/%Y').strftime('%Y-%m-%d')    
            
            
            numero_tipo_presupuesto_puesto = row['numero_tipo_presupuesto_puesto']                           
            if pd.isna(numero_tipo_presupuesto_puesto)  :
                numero_tipo_presupuesto_puesto = None     
            
            id_categoria_rubro = row['id_categoria_rubro']                           
            if pd.isna(id_categoria_rubro)  :
                id_categoria_rubro = None     
                
            id_cargo = row['id_cargo']                           
            if pd.isna(id_cargo)  :
                id_cargo = None     
                
            antig_anho_docente = row['antig_anho_docente']                           
            if pd.isna(antig_anho_docente)  :
                antig_anho_docente = None     
                
            antig_mes_docente = row['antig_mes_docente']                           
            if pd.isna(antig_mes_docente)  :
                antig_mes_docente = None     
                
            antig_anho_administrativo = row['antig_anho_administrativo']                           
            if pd.isna(antig_anho_administrativo)  :
                antig_anho_administrativo = None     
                
            antig_mes_administrativo = row['antig_mes_administrativo']                           
            if pd.isna(antig_mes_administrativo)  :
                antig_mes_administrativo = None     
                
            antig_anho_contrato = row['antig_anho_contrato']                           
            if pd.isna(antig_anho_contrato)  :
                antig_anho_contrato = None     
                
            antig_mes_contrato = row['antig_mes_contrato']                           
            if pd.isna(antig_mes_contrato)  :
                antig_mes_contrato = None     
                
            anos_antiguedad_oee = row['anos_antiguedad_oee']                           
            if pd.isna(anos_antiguedad_oee)  :
                anos_antiguedad_oee = None     
                
            meses_antiguedad_oee = row['meses_antiguedad_oee']                           
            if pd.isna(meses_antiguedad_oee)  :
                meses_antiguedad_oee = None     
                
            codigo_jurisdiccion_04 = row['codigo_jurisdiccion_04']                           
            if pd.isna(codigo_jurisdiccion_04)  :
                codigo_jurisdiccion_04 = None     
                
            codigo_institucion_planificacion = row['codigo_institucion_planificacion']                           
            if pd.isna(codigo_institucion_planificacion)  :
                codigo_institucion_planificacion = None     
                
            codigo_distrito = row['codigo_distrito']                           
            if pd.isna(codigo_distrito)  :
                codigo_distrito = None     
                
            codigo_localidad = row['codigo_localidad']                           
            if pd.isna(codigo_localidad)  :
                codigo_localidad = None     
                
            id_asignacion_presupuesto_linea = row['id_asignacion_presupuesto_linea']                           
            if pd.isna(id_asignacion_presupuesto_linea)  :
                id_asignacion_presupuesto_linea = None     
                
                
                
            fecha_puesto_efectivo = row['fecha_puesto_efectivo']
            if not isinstance(fecha_puesto_efectivo, str) or not fecha_puesto_efectivo.strip():  
                fecha_puesto_efectivo = None
            else:
                fecha_puesto_efectivo = datetime.strptime(fecha_puesto_efectivo, '%d/%m/%Y').strftime('%Y-%m-%d')          
                
                
            carga_horaria_semanal = row['carga_horaria_semanal']                           
            if pd.isna(carga_horaria_semanal)  :
                carga_horaria_semanal = None           
                
            

            # Crear la cadena SQL como una f-string
            insert_query = f"""
            INSERT INTO marcacion_ubicacion
                (codigo_puesto,  codigo_dependencia, nombre_dependencia, id_jurisdiccion, codigo_jurisdiccion_02,
                nombre_jurisdiccion_02, nombre_jurisdiccion_03, nombre_jurisdiccion_04, codigo_institucion, nombre_institucion,
                codigo_nivel_educativo, codigo_trabajador, letra_persona, fecha_nacimiento, direccion_persona,
                correo_electronico_persona, nombre_trabajador, fecha_ingreso, antig_anho, antig_mes,
                numero_turno, codigo_turno, nombre_cargo, id_seccion, letra_numero_seccion,
                codigo_seccion, cantidad_alumnos, numero_curso, codigo_curso, id_curriculo,
                nombre_curriculo, descripcion_curriculo, codigo_categoria_rubro, monto_categoria_rubro, horas_catedras,
                cantidad_concepto_puesto, asignacion, titulo_persona, codigo_tipo_categoria_rubro, numero_tipo_categoria_rubro,
                mes_asig_rubro, anio_asig_rubro, numero_matriculacion, carga_horaria_mensual, nombre_disciplina,
                numero_persona, numero_tipo_cargo, numero_tipo_gestion, numero_nivel_educativo, id_puesto,
                es_categoria_administrativa, codigo_tipo_cargo, es_administracion_central, codigo_cargo, numero_tipo_administracion,
                codigo_tipo_administracion, equivalencia_hora_turno,  codigo_tipo_gestion, codigo_tipo_zona,
                direccion_dependencia, telefono_dependencia, numero_tipo_institucion, codigo_tipo_institucion, codigo_tipo_presupuesto_puesto,
                codigo_dependencia_apn, nombre_dependencia_apn, numero_tipo_dependencia, codigo_tipo_dependencia, es_ad_honorem,
                es_voluntario, anio_ad_honorem, mes_ad_honorem, fecha_puesto_efectivo_verificado, numero_tipo_presupuesto_puesto,
                id_categoria_rubro, id_cargo, antig_anho_docente, antig_mes_docente, antig_anho_administrativo,
                antig_mes_administrativo, antig_anho_contrato, antig_mes_contrato, anos_antiguedad_oee, meses_antiguedad_oee,
                codigo_jurisdiccion_04, codigo_institucion_planificacion, codigo_distrito, codigo_localidad, id_asignacion_presupuesto_linea,
                fecha_puesto_efectivo, carga_horaria_semanal, anio, mes               
                )
            VALUES
                ( %s, %s, %s, %s, %s,   %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,    
                %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,      
                %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, 
                %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,
                %s, %s, %s, %s, %s,     %s, %s, %s, %s, %s,     %s, %s, %s, %s
                );
            """
            
            data = (
                codigo_puesto,  codigo_dependencia, nombre_dependencia, id_jurisdiccion, codigo_jurisdiccion_02,
                nombre_jurisdiccion_02, nombre_jurisdiccion_03, nombre_jurisdiccion_04, codigo_institucion, nombre_institucion,
                codigo_nivel_educativo, codigo_trabajador, letra_persona, fecha_nacimiento, direccion_persona,
                correo_electronico_persona, nombre_trabajador, fecha_ingreso, antig_anho, antig_mes, 
                numero_turno, codigo_turno, nombre_cargo, id_seccion, letra_numero_seccion,
                codigo_seccion, cantidad_alumnos, numero_curso, codigo_curso, id_curriculo,
                nombre_curriculo, descripcion_curriculo, codigo_categoria_rubro, monto_categoria_rubro, horas_catedras,
                cantidad_concepto_puesto, asignacion, titulo_persona, codigo_tipo_categoria_rubro, numero_tipo_categoria_rubro,
                mes_asig_rubro, anio_asig_rubro, numero_matriculacion, carga_horaria_mensual, nombre_disciplina,
                numero_persona, numero_tipo_cargo, numero_tipo_gestion, numero_nivel_educativo, id_puesto,
                es_categoria_administrativa, codigo_tipo_cargo, es_administracion_central, codigo_cargo, numero_tipo_administracion,
                codigo_tipo_administracion, equivalencia_hora_turno, codigo_tipo_gestion, codigo_tipo_zona,
                direccion_dependencia, telefono_dependencia, numero_tipo_institucion, codigo_tipo_institucion, codigo_tipo_presupuesto_puesto,
                codigo_dependencia_apn, nombre_dependencia_apn, numero_tipo_dependencia, codigo_tipo_dependencia, es_ad_honorem,
                es_voluntario, anio_ad_honorem, mes_ad_honorem, fecha_puesto_efectivo_verificado, numero_tipo_presupuesto_puesto,
                id_categoria_rubro, id_cargo, antig_anho_docente, antig_mes_docente, antig_anho_administrativo,
                antig_mes_administrativo, antig_anho_contrato, antig_mes_contrato, anos_antiguedad_oee, meses_antiguedad_oee,
                codigo_jurisdiccion_04, codigo_institucion_planificacion, codigo_distrito, codigo_localidad, id_asignacion_presupuesto_linea,
                fecha_puesto_efectivo, carga_horaria_semanal, anio_filtro, mes_filtro
                
            )
            
            
            # Imprime la consulta SQL con datos insertados
            # print(insert_query % data)
            
            # Ejecuta la consulta SQL contenida en 'insert_query' utilizando los datos en 'data'
            cursor.execute(insert_query, data)
            
            


        # Confirmar los cambios en la base de datos
        connection.commit()

        # Informar que la inserción se ha completado
        print("Registros insertados 3ra hoja")



        # Cerrar el cursor y la conexión
        cursor.close()
        connection.close()

        


        return True

    except Exception as e:
        # Manejar cualquier excepción y devolver False indicando error
        print(f"Error: {str(e)}")
        return False
