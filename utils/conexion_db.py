from django.db import  connections, OperationalError


# Función para establecer la conexión a la base de datos
def establecer_conexion():
    try:
        # Utilizar la conexión de Django
        django_connection = connections['default']
        django_connection.ensure_connection()

        print("Conexión exitosa a la base de datos")
        return django_connection
    except OperationalError as err:
        # Manejar errores y mostrar un mensaje de error
        print(f"Error al conectarse a la base de datos: {err}")
        return None

if __name__ == "__main__":
    establecer_conexion()
