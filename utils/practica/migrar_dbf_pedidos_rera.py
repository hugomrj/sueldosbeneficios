import argparse
from dbfread import DBF
import sys
from utils.conexion_db import establecer_conexion


# Crear un objeto ArgumentParser
parser = argparse.ArgumentParser(description='Script para filtrar registros DBF.')
parser.add_argument('--anio', type=int, help='Año:', required=True)
parser.add_argument('--mes', type=int, help='Mes:', required=True)
args = parser.parse_args()

# Ahora puedes acceder a los valores de anio y mes a través de args.anio y args.mes
anio_filtro = args.anio
mes_filtro = args.mes


# Establece la conexión a la base de datos
conn = establecer_conexion()

# Crear un cursor para ejecutar consultas SQL
cursor = conn.cursor()


# Ruta al archivo .dbf

# dbf_file = r'C:\Users\Usuario\Documents\Sueldos y Beneficios\RERA\remuneraciones_pedidos.dbf'

dbf_file = r'C:\Users\Usuario\Desktop\pedidos_012024.DBF'


# Leer el archivo .dbf
table = DBF(dbf_file, load=True)

# Inicializar un contador
contador = 1
 
# Ahora puedes acceder a los valores de anio y mes a través de args.anio y args.mes
#anio_filtrar = args.anio
#mes_filtrar = args.mes


# Leer el archivo .dbf
table = DBF(dbf_file, load=True)

# Filtrar los registros en memoria
registros_filtrados = [
    record for record in table
    if record['ANIO'] == anio_filtro and record['MES'] == mes_filtro
]


# Recorrer y procesar los registros filtrados
for record in registros_filtrados:
    # print(record)
    
    anio = record['ANIO']
    mes = record['MES']
    cedula = record['CEDULA']
    nombre = record['NOMBRE']
    objeto = record['OBJETO']
    id_depende = record['ID_DEPENDE']
    horas = record['HORAS']
    horas_ra = record['HORAS_RA']
    expediente = record['EXPEDIENTE']
    memorandum = record['MEMORANDUM']
    actualizo = record['ACTUALIZO']
    observacion = record['OBSERVACIO']
    usuario = record['USUARIO']
    cod_og = record['COD_OG']
    encontro = record['ENCONTRO']
    
    
    
    
    # Crear la cadena SQL como una f-string
    insert_query = f"""
    INSERT INTO django_sueldosbeneficios.remuneraciones_pedidos
        (
        anio, mes, cedula, nombre, objeto, 
        id_depende, horas, horas_ra, expediente, memorandum, 
        actualizo, observacion, usuario, cod_og, encontro
        )
    VALUES
        (   %s, %s, %s, %s, %s, 
            %s, %s, %s, %s, %s, 
            %s, %s, %s, %s, %s
        );
    """
           
    data = (
        anio , mes , cedula , nombre ,objeto ,
        id_depende , horas , horas_ra , expediente , memorandum ,
        actualizo , observacion , usuario , cod_og , encontro
        )           
    
    # Imprime la consulta SQL con datos insertados
    # print(insert_query % data)    
    
    # Ejecuta la consulta SQL contenida en 'insert_query' utilizando los datos en 'data'
    cursor.execute(insert_query, data)

    
    print(f"Registro {contador}:")        
    print("\n")          
    contador += 1
    
# Confirmar los cambios en la base de datos
conn.commit()    