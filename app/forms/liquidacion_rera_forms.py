
from django import forms

from app.models.liquidacion_rera_model import LiquidacionRERA





class LiquidacionRERAForm(forms.ModelForm):
    
    class Meta:
        model = LiquidacionRERA
        fields = '__all__'