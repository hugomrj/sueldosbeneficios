
from django import forms

from app.models.remuneracion_pedido_model import RemuneracionPedido




class RemuneracionesPedidosForm(forms.ModelForm):
        
    observacion = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = RemuneracionPedido
        fields = '__all__'