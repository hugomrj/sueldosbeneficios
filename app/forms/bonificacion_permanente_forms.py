



from django import forms

from app.models.bonificacion_permanente_model import BonificacionPermanente
from app.models.objeto_gasto_model import ObjetoGasto



class BonificacionPermanenteForm(forms.ModelForm):

    class Meta:
        model = BonificacionPermanente
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["objeto"].queryset = ObjetoGasto.objects.filter(
            codigo_objeto_gasto__in=[113, 133, 199]
        )
