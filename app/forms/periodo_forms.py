
from django import forms

from app.models.periodo_model import Periodo


class PeriodoForm(forms.ModelForm):        
    
    class Meta:
        model = Periodo
        fields = '__all__'        


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.pk:  
            self.fields['numero'].initial = 1


    def clean(self):
        cleaned_data = super().clean()
                        
        anio = cleaned_data.get('anio')
        if not anio:
            raise forms.ValidationError("El año no puede estar vacío")        

        numero = cleaned_data.get('numero')
        if not numero:
            raise forms.ValidationError("Número planilla no puede estar vacío")
        elif not str(numero).isdigit():
            raise forms.ValidationError("Número planilla debe ser numérico")


        objeto = cleaned_data.get('objeto')
        if not objeto:
            raise forms.ValidationError("El objeto no puede estar vacío")
        
        concepto = cleaned_data.get('concepto')
        if not concepto:
            raise forms.ValidationError("El concepto no puede estar vacío")
        



        return cleaned_data
       
             
        
    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        if instance.cerrado == 1:            
            self.add_error(None, "No se puede editar un registro cerrado.")
        return instance
    
  