


from django import forms

from app.models.bonificacion_pedido_model import BonificacionPedido
from app.models.estado_model import Estado
from app.models.objeto_gasto_model import ObjetoGasto



class BonificacionPedidoForm(forms.ModelForm):

    class Meta:
        model = BonificacionPedido
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.pk:  # Verifica si no es una instancia existente
            try:
                estado_predeterminado = Estado.objects.get(
                    pk=1
                )  # Asume que el estado 'habilitado' tiene pk=1
                self.fields["estado"].initial = estado_predeterminado
            except Estado.DoesNotExist:
                pass

            self.fields["subconcepto"].initial = 1
            self.fields["objeto"].queryset = ObjetoGasto.objects.filter(
                codigo_objeto_gasto__in=[113, 133, 199]
            )

        # Hacer que el campo 'ccargo' no sea obligatorio
        
        if 'ccargo' in self.fields:
            self.fields['ccargo'].required = False
        

    def clean_ccargo(self):
        ccargo = self.cleaned_data.get('ccargo')
        if not ccargo:  # Verifica si el campo está vacío o es None
            raise forms.ValidationError("El campo 'Cargo efectivo' es obligatorio.")
        return ccargo            


    '''
    def clean(self):
        cleaned_data = super().clean()
        dpto = cleaned_data.get("dpto")

        if dpto is None:
            raise forms.ValidationError({"dpto": 'El campo "dpto" no puede ser null.'})

        return cleaned_data
    '''
