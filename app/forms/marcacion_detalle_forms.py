
from django import forms

from app.models.marcacion_detalle_model import MarcacionDetalle





class MarcacionDetalleForm(forms.ModelForm):
        
    observacion = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = MarcacionDetalle
        fields = '__all__'