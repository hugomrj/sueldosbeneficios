from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.db import IntegrityError, connection

from django.urls import reverse
from app.forms.bonificacion_pedido_forms import BonificacionPedidoForm
from app.models.bonificacion_pedido_model import BonificacionPedido
from app.models.concepto_nomina_model import ConceptoNomina
from app.models.objeto_gasto_model import ObjetoGasto
from app.models.tipo_cargo_model import TipoCargo
from app.models.trabajador_model import Trabajador
from config import settings
from datetime import datetime as dt

from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.utils.safestring import mark_safe

from django.db.models import F, Value, Func, Case, When, CharField
from django.db.models.functions import Length, Trim



class BonificacionPedidoList (LoginRequiredMixin, View):

    template_name = "app/bonificacion_pedido/list.html"
    items_por_pagina = settings.ITEMS_POR_PAGINA

    def get(self, request, anio=None,  obj=None, concepto=None ):
        # Obtener el año actual
        anio_actual = dt.now().year

        # Asignar valores por defecto
        anio = anio or anio_actual

        if obj is None:
            obj = 0

        # Capturar el parámetro 'cedula' de la consulta, si existe
        cedula = request.GET.get("cedula")

        # Filtrar registros por año y opcionalmente por 'cedula'
        registros = BonificacionPedido.objects.filter(anio=anio)


        # Filtro para 'objeto' y 'concepto' juntos
        if obj != 0 or concepto != 0:
            registros = BonificacionPedido.objects.all()  # Iniciar la consulta
            if obj != 0:
                registros = registros.filter(objeto=obj)
            if concepto != 0:
                registros = registros.filter(concepto=concepto)

            # Filtrar conceptos según el objeto recibido
            conceptos = ConceptoNomina.objects.filter(og=obj)

        # Filtro independiente para 'cedula'
        if cedula:
            registros = BonificacionPedido.objects.filter(cedula=cedula)

        registros = registros.order_by("id")

        # Paginación
        paginator = Paginator(registros, self.items_por_pagina)
        page = request.GET.get("page")


        

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            # Si el parámetro de página no es un entero, mostrar la primera página
            lista = paginator.page(1)
        except EmptyPage:
            # Si la página está fuera de rango, mostrar la última página
            lista = paginator.page(paginator.num_pages)



        # Realiza la consulta de objetos reales
        objetos = list(ObjetoGasto.objects.filter(codigo_objeto_gasto__in=[113, 133, 199]))
        # Crea un objeto temporal para representar el filtro "Todos los objetos"
        todos_objetos = ObjetoGasto(codigo_objeto_gasto=0, nombre_objeto_gasto="Todos los objetos")
        # Inserta este objeto al inicio de la lista
        objetos.insert(0, todos_objetos)
        
        
        # Pasar la lista de marcaciones paginada al contexto
        contexto = {
            "lista": lista,
            "anio": anio,
            "role_name": request.role,
            "cedula": cedula,
            'objetos': objetos,
            "obj": obj,
            "concepto_id" : concepto ,
            "conceptos" : conceptos 
            
        }

        return render(request, self.template_name, contexto)






class BonificacionPedidoCreate (LoginRequiredMixin, View):
    template_name = "app/bonificacion_pedido/create.html"

    def get(self, request, *args, **kwargs):

        anio = self.kwargs.get("anio")


        # Obtener opciones para el select
        with connection.cursor() as cursor:            
            sql_query = """
            
            SELECT 
                id, CONCAT(cargocat, '     ', imputacion) AS descripcion
            FROM (
                SELECT  
                    id, 
                    CONCAT(cp, '-', pg, '-', sp, '-', pry, '-', obj, '-', ff, '-', of, '-', dpto, '-', linea)
                    AS imputacion,
                    CONCAT(descripcion, '  ( ', categoria, ' )') AS cargocat
                FROM anexo113
            ) ta 
            order by cargocat, imputacion ;
            
            """                        
            cursor.execute(sql_query)
            opciones_cargo113 = cursor.fetchall()


        sql = """
        SELECT tipo_cargo,             
            CASE 
                WHEN TRIM(categoria) = '' THEN NOMBRE_CARGO
                ELSE CONCAT(categoria, ' - ', NOMBRE_CARGO)
            END AS descripcion
        FROM tipo_cargo
        WHERE categoria IS NOT NULL
        ORDER BY tipo_cargo;
        """
        
        # Ejecutamos la consulta
        with connection.cursor() as cursor:
            cursor.execute(sql)
            tipo_cargos = cursor.fetchall()
        
        form = BonificacionPedidoForm()
        contexto = {
            "form": form,
            "anio": anio,
            "role_name": request.role,
            "opciones_cargo113": opciones_cargo113,
            "tipo_cargos": tipo_cargos
        }

        return render(request, self.template_name, contexto)



    def post(self, request, anio):

        form = BonificacionPedidoForm(request.POST)


        try:

            if form.is_valid():

                
                concepto = request.POST.get("concepto")
                

                # Obtén la instancia del trabajador usando la cédula
                cedula = request.POST.get("cedula")
                trabajador = get_object_or_404(Trabajador, cedula=cedula)

                objeto_codigo = request.POST.get("objeto")


                # Verificación de objeto_codigo
                if objeto_codigo in ['113', '133', '199']:

                    registro = form.save(commit=False)
                    registro.anio = anio
                    registro.nombre = trabajador.nombre

                    if objeto_codigo == '113':
                        # Cargar o no ciertos campos específicos para '113'
                        # Lógica para '113'
                        #raise ValueError("Se ha generado un error deliberado para probar la carga")

                        # imputacion  
                        # Obtén el valor de cargo113
                        cargo113 = request.POST.get("cargo113")
                        with connection.cursor() as cursor:
                            sql_query = """
                            SELECT CONCAT(cp, '-', pg, '-', sp, '-', obj, '-', ff, '-', of, '-', dpto) AS imputacion
                            FROM anexo113
                            WHERE id = %s;
                            """                        
                            cursor.execute(sql_query, [cargo113])
                            
                            # Obtén un solo registro
                            resultado = cursor.fetchone()
                        # Asigna el valor de imputacion a la variable
                        imputacion_value = resultado[0] if resultado else None
                        registro.imputacion = imputacion_value
                        registro.tipo_cargo = None

                        pass


                    elif objeto_codigo == '133':
                        # Cargar o no ciertos campos específicos para '133'
                        # Lógica para '133'
                        tipo_cargo = request.POST.get("tipo_cargo")


                        if tipo_cargo:
                            registro.tipo_cargo = int(tipo_cargo)
                            if registro.tipo_cargo in [7, 8, 9]:  # Si tipo_cargo es 7, 8 o 9
                                registro.porcentaje = 50
                            else:  # En cualquier otro caso
                                registro.porcentaje = 30

                        registro.cargo113 = None


                        pass
                    elif objeto_codigo == '199':
                        # Cargar o no ciertos campos específicos para '199'
                        # Lógica para '199'
                        pass
                else:
                    # Si el objeto_codigo no es válido
                    return JsonResponse({"error": "Código de objeto inválido."}, status=400)

                
                registro.save()

                message = "El registro se ha agregado correctamente."
                messages.success(request, message)
                
                

                return redirect("bonificacion_pedido:list", 
                                anio=anio, 
                                obj=objeto_codigo, 
                                concepto=concepto )

            else:

                # Si el formulario no es válido, accede a los errores del formulario
                if form.errors:
                    error_message = "Hubo un error al agregar el registro"

                    # Si el formulario no es válido, accede a los errores
                    if form.errors.get("__all__"):
                        error_message = form.errors["__all__"][0]
                    else:
                        error_message = "Hubo un error al agregar el registro:<br><ul>"
                        for field, field_errors in form.errors.items():
                            for field_error in field_errors:
                                # Mostrar solo el mensaje de error sin el nombre del campo
                                error_message += f"<li>{field_error}</li>"
                        error_message += "</ul>"

                    # Muestra el mensaje de error sin los valores POST
                    messages.error(request, mark_safe(error_message))


        except IntegrityError as e:
            # Captura errores de integridad de la base de datos
            messages.error(request, f"Error de integridad: {str(e)}")


        except ValueError as e:
            # Captura el error generado manualmente
            messages.error(request, str(e))


        tipo_cargos = TipoCargo.objects.filter(clave__isnull=False).order_by('tipo_cargo').values_list('tipo_cargo', 'nombre_cargo')

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {
            "form": form, 
            "anio": anio, 
            "role_name": request.role,
            "tipo_cargos": tipo_cargos,
            "concepto": concepto
            }

        return render(request, self.template_name, contexto)







class BonificacionPedidoDetail (LoginRequiredMixin, View):
    template_name = "app/bonificacion_pedido/detail.html"

    def get(self, request, pk):
        registro = get_object_or_404(BonificacionPedido, id=pk)
        form = BonificacionPedidoForm(instance=registro)

        anio = registro.anio


        # Obtener opciones para el select
        with connection.cursor() as cursor:            
            sql_query = """
            SELECT 
                id, CONCAT(cargocat, '     ', imputacion) AS descripcion
            FROM (
                SELECT  
                    id, 
                    CONCAT(cp, '-', pg, '-', sp, '-', pry, '-', obj, '-', ff, '-', of, '-', dpto) AS imputacion,
                    CONCAT(descripcion, '  ( ', categoria, ' )') AS cargocat
                FROM anexo113
            ) ta 
            order by cargocat ;
            """                        
            cursor.execute(sql_query)
            opciones_cargo113 = cursor.fetchall()


        contexto = {
            "form": form,
            "registro": registro,
            "anio": anio,
            "role_name": request.role,
            "opciones_cargo113": opciones_cargo113,
        }

        return render(request, self.template_name, contexto)







class BonificacionPedidoEdit (LoginRequiredMixin, View):
    template_name = "app/bonificacion_pedido/edit.html"

    def get(self, request, pk):
        registro = get_object_or_404(BonificacionPedido, id=pk)
        form = BonificacionPedidoForm(instance=registro)

        # Imprimir los datos del formulario
        """
        print("Datos del formulario:")
        for field_name, field_value in form.fields.items():
            print(f"{field_name}: {form.initial.get(field_name)}")   
        """

        anio = registro.anio

        contexto = {
            "form": form,
            "registro": registro,
            "anio": anio,
            "role_name": request.role,
        }
        return render(request, self.template_name, contexto)




    def post(self, request, pk):

        registro = get_object_or_404(BonificacionPedido, id=pk)
        form = BonificacionPedidoForm(request.POST, instance=registro)
        anio = registro.anio

        trabajador = get_object_or_404(Trabajador, cedula=registro.cedula)

        # Obtener el valor de 'objeto' desde el POST
        objeto_codigo = request.POST.get('objeto')

        if form.is_valid():
            registro = form.save(commit=False)
            registro.anio = anio
            registro.nombre = trabajador.nombre
            # datodb = BonificacionesPedidos.objects.get(pk=pk)
            concepto_codigo = registro.concepto.codigo_concepto_nomina

            registro.save()

            message = "El registro se ha editado correctamente."
            messages.success(request, message)

            return redirect("bonificacion_pedido:list", 
                            anio=anio, 
                            obj=objeto_codigo, 
                            concepto=concepto_codigo) 
        
        else:
            # Si el formulario no es válido, accede a los errores y envía un mensaje de error detallado
            errors = form.errors.as_data()
            message = (
                "Hubo un error al editar el registro. Por favor, verifica los datos:"
            )
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    message += f"\n{field.capitalize()}: {field_error.message}"
            messages.error(request, message)

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {
            "form": form,
            "registro": registro,
            "anio": anio,
            "role_name": request.role,
        }

        return render(request, self.template_name, contexto)







class BonificacionPedidoDelete (LoginRequiredMixin, View):

    def get(self, request, pk):
        remuneracion = get_object_or_404(BonificacionPedido, pk=pk)
        
        
        anio = remuneracion.anio
        
        # Si quieres el 'codigo_objeto_gasto' (atributo específico del objeto relacionado)
        codigo_objeto_gasto = remuneracion.objeto.codigo_objeto_gasto        
        concepto_codigo = remuneracion.concepto.codigo_concepto_nomina

        
        remuneracion.delete()

        # Agrega un mensaje de éxito
        messages.success(request, "El registro ha sido eliminado correctamente.")

        # Utiliza reverse para obtener la URL de la vista con los argumentos
        url = reverse("bonificacion_pedido:list", args=(
                                                    anio,
                                                    codigo_objeto_gasto,
                                                    concepto_codigo ))

        # Redirige a la URL obtenida
        return redirect(url)
