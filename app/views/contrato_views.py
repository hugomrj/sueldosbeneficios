import os

from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.contrib import messages

from config import settings
from utils.contratados_excel import migrar_contratados_excel


    


class ContratadoMigrar (LoginRequiredMixin, View):
    
    template_name = 'contratados_subirarchivo.html'
    
    def get(self, request):
        
        context = {
            'role_name': request.role
        }        
        return render(request, self.template_name, context)
    
    
   






    def post(self, request):
        if 'excel_file' in request.FILES:
            excel_file = request.FILES['excel_file']
            anio = request.POST.get('anio')
            mes = request.POST.get('mes')

            # Garantizar que la carpeta "uploads" exista
            destination_folder = os.path.join(settings.MEDIA_ROOT, 'uploads')
            os.makedirs(destination_folder, exist_ok=True)

            # Guardar el archivo en el servidor
            save_path = os.path.join(destination_folder, excel_file.name)
            with open(save_path, 'wb') as destination:
                for chunk in excel_file.chunks():
                    destination.write(chunk)

            
            # Llamada a la función migrar_contratados_excel con la ruta del archivo, el año y el mes como argumentos
            exito = migrar_contratados_excel(save_path, anio, mes)

            # Verificar si la operación fue exitosa utilizando el valor devuelto por la función
            if exito:
                # Si la operación fue exitosa
                messages.success(request, 'Migracion completa')
            else:
                # Si la operación no fue exitosa
                messages.error(request, 'Error en el procesamiento del archivo.')


        return render(request, self.template_name)            
            
        