import json
from django.db import connection
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render

from django.urls import reverse
from app.forms.bonificacion_permanente_forms import BonificacionPermanenteForm
from app.models.beneficio_estado import BeneficioEstado
from app.models.bonificacion_permanente_model import BonificacionPermanente
from app.models.concepto_nomina_model import ConceptoNomina
from app.models.objeto_gasto_model import ObjetoGasto
from app.models.proceso_model import Proceso
from app.models.trabajador_model import Trabajador
from app.reports.bonificacion_permanente_reports import generar_bonificacion_permanente_excel
from config import settings
from datetime import datetime as dt


from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q


from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator




# Tabla bonificaciones

class BonificacionPermanenteList (LoginRequiredMixin, View ):
    
    template_name = 'app/bonificacion_permanente/list.html'
    items_por_pagina = settings.ITEMS_POR_PAGINA

    def get(self, request, anio=None, mes=None, obj=None, concepto=None):
        # Obtener el año actual
        anio_actual = dt.now().year


        # Asignar valores por defecto
        mes = mes or 1
        anio = anio or anio_actual
        coleccion = BonificacionPermanente.objects.filter(anio=anio, mes=mes).order_by('cedula')
               

        if obj is None:
            obj = 0
        
        if concepto is None:
            concepto = '0'
            
        # Obtener el valor del parámetro 'q' de la URL
        query_param = request.GET.get('q', '')
        
        # Filtrar la colección basada en los parámetros
        coleccion = BonificacionPermanente.objects.filter(
            anio=anio,
            mes=mes,
            objeto=obj,
            concepto=concepto
        ).order_by('anio', 'id')
        

        # Si hay un valor en el parámetro 'q', realizar la búsqueda en cedula o nombre
        if query_param:
            coleccion = coleccion.filter(
                Q(cedula__icontains=query_param) |
                Q(nombre__icontains=query_param)
            )
        # Calcula el conteo de elementos de la consulta
        cantidad_registros = coleccion.count()            
           

        # Paginación
        paginator = Paginator(coleccion, self.items_por_pagina)
        page = request.GET.get('page')

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            # Si el parámetro de página no es un entero, mostrar la primera página
            lista = paginator.page(1)
        except EmptyPage:
            # Si la página está fuera de rango, mostrar la última página
            lista = paginator.page(paginator.num_pages)


        # Obtener todos los procesos para el combo select        
        procesos = Proceso.objects.filter(proceso__in=[1])
        
        
        # Obtener todos los objetos de gasto y conceptos de nómina para los combos select
        # objetos = ObjetoGasto.objects.all()
        objetos = ObjetoGasto.objects.filter(codigo_objeto_gasto__in=[113, 133, 199])
        # conceptos = ConceptoNomina.objects.all()   
        # conceptos = ConceptoNomina.objects.filter(codigo_concepto_nomina=concepto)
        conceptos = ConceptoNomina.objects.filter(og=obj)
        
        concepto_id = concepto

        contexto = {
            'lista': lista,
            'anio': anio,
            'mes': mes,
            'role_name': request.role,
            'cantidad_registros': cantidad_registros,
            'procesos': procesos,
            'objetos': objetos,
            'conceptos': conceptos,
            'concepto_id': concepto_id,
            'obj': obj
        }
                        
        return render(request, self.template_name, contexto)
        



    def post(self, request, anio=None, mes=None, obj=None, concepto=None):
        
        # Obtener los datos enviados desde JavaScript
        data = json.loads(request.body.decode('utf-8'))  # Decodificar el cuerpo de la solicitud
        mensaje = data.get('mensaje')
        tipo = data.get('tipo')  # Tipo de mensaje: success, error, warning, info

        # Agregar el mensaje según el tipo recibido desde JavaScript
        if tipo == 'success':
            messages.success(request, mensaje)
        elif tipo == 'error':
            messages.error(request, mensaje)
        elif tipo == 'warning':
            messages.warning(request, mensaje)
        else:
            messages.info(request, mensaje)

        # Redirigir y almacenar los mensajes en la sesión
        messages_store = messages.get_messages(request)
        for message in messages_store:
            messages.add_message(request, message.level, message.message)

        # Determinar la URL de redirección
        redirect_url = reverse('bonificacion_permanente:list')

        # Devolver una respuesta JSON con la URL de redirección
        return JsonResponse({'redirect_url': redirect_url})










    
class BonificacionPermanenteEdit (LoginRequiredMixin, View):
    template_name = "app/bonificacion_permanente/edit.html"

    def get(self, request, pk):        
        registro = get_object_or_404(BonificacionPermanente, id=pk)
        form = BonificacionPermanenteForm(instance=registro)

        anio = registro.anio
        mes = registro.mes
        
        # Obtener la representación del objeto como 'codigo - nombre'
        objeto = str(registro.objeto) if registro.objeto else ""      
        concepto = str(registro.concepto) if registro.concepto else ""      
        
        estados = BeneficioEstado.objects.exclude(estado=2) 

        contexto = {
            "form": form,
            "registro": registro,
            "anio": anio,
            "mes": mes,
            "objeto": objeto, 
            "concepto": concepto,
            "estados": estados,
            "role_name": request.role,
        }
        return render(request, self.template_name, contexto)




    def post(self, request, pk):
        # Obtener el estado del POST
        estado_id = request.POST.get('estado')

        # Mostrar el estado en consola
        print(f"Estado recibido: {estado_id}")







        # Filtrar el objeto BonificacionPermanente usando el id del POST
        try:
            bonificacion = BonificacionPermanente.objects.get(id=pk)


            # Obtener los valores necesarios para la redirección
            anio = request.POST.get('anio')  # También puedes obtenerlo de `bonificacion` si está en el modelo
            mes = request.POST.get('mes')    # Igual que arriba
            objeto = request.POST.get('obj_hidden')
            concepto = request.POST.get('concepto_id')

            # Construir la URL manualmente con los valores necesarios
            url_redireccion = f'/bonificacion_permanente/list/{anio}/{mes}/{objeto}/{concepto}/'





            # Verificar si el estado actual es 2
            if bonificacion.estado.estado == 2:  # Suponiendo que 'estado' es una relación al modelo 'BeneficioEstado'
                messages.error(request, "No se puede actualizar un registro ya pagado.")
                return redirect(url_redireccion)

            # Obtener la instancia de BeneficioEstado que corresponde al estado_id
            estado = get_object_or_404(BeneficioEstado, estado=estado_id)

            # Asignar la instancia de BeneficioEstado al campo 'estado' del objeto BonificacionPermanente
            bonificacion.estado = estado

            # Guardar los cambios
            bonificacion.save()

            # Añadir un mensaje de éxito
            messages.success(request, 'Estado del registro editado correctamente.')

            return redirect(url_redireccion)
        except BonificacionPermanente.DoesNotExist:
            print(f'No se encontró ninguna bonificación con el id {pk}')
        
        # Redirigir a la lista después de procesar si algo falla
        return redirect('bonificacion_permanente:list')







class BonificacionPermanenteDetail(LoginRequiredMixin, View):    
    template_name = "app/bonificacion_pedido/edit.html"

    def get(self, request, pk):
        registro = get_object_or_404(BonificacionPermanente, id=pk)
        form = BonificacionPermanenteForm(instance=registro) 


        # Imprimir todos los campos y valores de registro
        print(vars(registro))

        
        anio = registro.anio
        mes = registro.mes
        obj = registro.objeto_id
        concepto_id = registro.concepto_id

        objeto = get_object_or_404(ObjetoGasto, codigo_objeto_gasto=obj)
        concepto = get_object_or_404(ConceptoNomina, codigo_concepto_nomina=concepto_id)
       

        contexto = {
            'form': form, 
            'anio': anio, 
            'mes': mes,                                                                      
            'obj': obj,  
            'objeto': objeto,  
            'concepto_id' : concepto_id,                         
            'concepto' : concepto,             
            'role_name': request.role
            }             

        
        return render(request, self.template_name, contexto)
    
    
    
    
    
    
class BonificacionPermanenteTablePartialView(LoginRequiredMixin, View):
    template_name = 'app/bonificacion_permanente/table_partial.html'
    
    def get(self, request):
        # Obtiene los parámetros de consulta 'anio' y 'mes' del request
        anio = request.GET.get('anio')
        mes = request.GET.get('mes')
        
        if anio and mes:
            # Si ambos parámetros están presentes, filtra los datos
            bonificaciones = BonificacionPermanente.objects.filter(anio=anio, mes=mes)
        else:
            # Si faltan, retorna un queryset vacío
            bonificaciones = BonificacionPermanente.objects.none()
        
        context = {
            'bonificaciones': bonificaciones,
            'anio': anio,
            'mes': mes
        }
        
        return render(request, self.template_name, context)
    
    
    
    
def exportar_excel_bonificacion(request):
    # Capturar los parámetros de la URL
    anio = request.GET.get('anio')
    mes = request.GET.get('mes')
    objeto = request.GET.get('objeto')
    concepto = request.GET.get('concepto')
    
    # Validar si el objeto es 113
    if objeto == '113':
        # Llamar a la función para generar el reporte
        return generar_bonificacion_permanente_excel(anio, mes, objeto, concepto)
    else:
        # Retornar un error si el objeto no es 113
        return HttpResponseBadRequest("falta generrar proceso de generacion de excel para 133")