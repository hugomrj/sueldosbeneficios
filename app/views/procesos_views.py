import json
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render

from django.urls import reverse
from app.services.procesos import proceso_numero_1, proceso_numero_2, proceso_numero_3
from config import settings
from datetime import datetime as dt


from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q


from django.contrib import messages










def generar_lista(request):
        
    print("generar lista")
    
    # Obtener el valor de selected_process del cuerpo de la solicitud POST
    selected_process = request.POST.get('selected_process')
    
    
    # Después de ejecutar el proceso, redirigir a la lista de bonificaciones
    anio = request.POST.get('anio')  
    mes = request.POST.get('mes')    
    objeto = request.POST.get('objeto')    
    concepto = request.POST.get('concepto')    

    
    # Verificar qué acción tomar según el valor de selected_process
    if selected_process == '0':
        messages.error(request, "No se selecciono proceso")
                
        
    elif selected_process == '1':                            
        proceso_numero_1(request, anio, mes, objeto, concepto)
        
        
    else:
        # Si selected_process no es ni '0' ni '1', manejar otros casos aquí
        messages.warning(request, "No reconocido")    
        

    # Determinar la URL de redirección basada en si anio y mes están presentes
    if anio and mes:
        # Utiliza reverse para obtener la URL de la vista con los argumentos        
        url = reverse('bonificacion_permanente:list', args=(anio, mes, objeto, concepto))            
    else:
        # Si anio o mes es None, redirigir a la vista de lista sin argumentos
        url = reverse('bonificacion_permanente:list')


    # Redirigir y almacenar los mensajes en la sesión
    messages_store = messages.get_messages(request)
    for message in messages_store:
        messages.add_message(request, message.level, message.message)


    # Redirige a la URL obtenida
    return redirect(url)











def generar_liquidacion_periodo(request):
        
    print("generar liquidacion periodo")

    
    if request.method == 'POST':
                
        selected_process = request.POST.get('selected_process')
        periodo = request.POST.get('periodo')
        
        # Aquí puedes procesar los valores de `selected_process` y `periodo`
        print(f"Proceso seleccionado: {selected_process}, Periodo: {periodo}")    
                    
        
        # Verificar qué acción tomar según el valor de selected_process
        if selected_process == '0':
            messages.error(request, "No se selecciono proceso")                    
            
        elif selected_process == '2':                           
            proceso_numero_2(request, periodo)
            url = reverse('bonificacion_permanente_pagada:list')    
            
        elif selected_process == '3':         
            proceso_numero_3(request, periodo)
            url = reverse('bonificacion_permanente_pagada:list')    

        else:
            # Si selected_process no es ni '0' ni '1', manejar otros casos aquí
            messages.warning(request, "No reconocido")    


        
        
        # Bloque separado para redirigir
        if periodo is not None and periodo != '0':
            # Redirigir con periodo
            url = reverse('bonificacion_permanente_pagada:list', kwargs={'periodo': periodo})
            return redirect(url)  # Redirigir a la nueva URL con periodo
        else:
            # Redirigir sin parámetros
            return redirect(reverse('bonificacion_permanente_pagada:list'))  # Redirigir sin parámetros
        
        



