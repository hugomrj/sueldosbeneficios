
# app/views/error_views.py
from django.shortcuts import render


# no se usa 
def error_permisos(request):
    """
    Vista personalizada para manejar el error 403.
    """
    error_message = "No tienes permisos para realizar esta acción."
    return render(request, 'error_popup.html', {'error_message': error_message})
