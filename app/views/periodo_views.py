from datetime import datetime as dt

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import connections
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from app.forms.periodo_forms import PeriodoForm
from app.models.concepto_nomina_model import ConceptoNomina
from app.models.objeto_gasto_model import ObjetoGasto
from app.models.periodo_model import Periodo











class PeriodoList (LoginRequiredMixin, View ):
    
    template_name = "app/periodo/list.html"
    
    items_por_pagina = settings.ITEMS_POR_PAGINA

    def get(self, request, anio=None):
        # Obtener el año actual
        anio_actual = dt.now().year

        print("--- fin de lista ---")

        # Asignar valores por defecto        
        anio = anio or anio_actual
        # coleccion = Periodo.objects.filter(anio=anio).order_by('id')
        # Obtener todos los registros de Periodo ordenados por 'id' de mayor a menor
        coleccion = Periodo.objects.all().order_by('-id')



        # Calcula el conteo de elementos de la consulta
        cantidad_registros = coleccion.count()   

        # Paginación
        paginator = Paginator(coleccion, self.items_por_pagina)
        page = request.GET.get('page')

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            # Si el parámetro de página no es un entero, mostrar la primera página
            lista = paginator.page(1)
        except EmptyPage:
            # Si la página está fuera de rango, mostrar la última página
            lista = paginator.page(paginator.num_pages)

  
        # Pasar la lista de marcaciones paginada al contexto
        contexto = {
            'lista': lista, 
            'anio': anio,           
            'cantidad_registros': cantidad_registros,
            'role_name': request.role
            }

        return render(request, self.template_name, contexto)
    





@method_decorator(permission_required('periodo.add_periodo', raise_exception=True), name='dispatch')
class PeriodoCreate (LoginRequiredMixin, View):
    
    template_name = "app/periodo/create.html"
    
    def get(self, request, *args, **kwargs):  
              
        anio = self.kwargs.get('anio')                
        
        # Obtener todos los objetos de gasto y conceptos de nómina para los combos select
        # objetos = ObjetoGasto.objects.all()
        # conceptos = ConceptoNomina.objects.all()     
        
        objetos = list(ObjetoGasto.objects.filter(codigo_objeto_gasto__in=[113, 133, 199]))

        form = PeriodoForm()                
        contexto = { 
            'form': form, 
            'anio': anio,
            'objetos': objetos,              
            'role_name': request.role
            }                        
        return render(request, self.template_name, contexto )
    
    
    def post(self, request, anio):        
        form = PeriodoForm(request.POST)                        
        data = request.POST                
                       
        if form.is_valid():   
                        
            tipo_funci = form.cleaned_data['tipo_funci']
            registro = form.save(commit=False)
            registro.anio = anio
            registro.tipo_funci = tipo_funci                                                                     
            registro.save()
                        
            message = 'El registro se ha agregado correctamente.'
            messages.success(request, message )

            return redirect('periodo:list', anio=anio)        
        
        else:
                            
            # Si el formulario no es válido, accede a los errores
            if form.errors.get('__all__'):
                # Si hay errores específicos del método clean, mostrar solo esos errores
                error_message = form.errors['__all__'][0]
            else:
                # Si no hay errores específicos del método clean, mostrar los errores generales del formulario
                error_message = 'Hubo un error al agregar el registro, '
                for field, field_errors in form.errors.items():
                    for field_error in field_errors:
                        error_message += f'\n{field.capitalize()}: {field_error}' 
            # messages.error(request, message)
            messages.error(request, error_message)              
            
                
            objetos = list(ObjetoGasto.objects.filter(codigo_objeto_gasto__in=[113, 133, 199]))
            # conceptos = ConceptoNomina.objects.all()       
            conceptos = ConceptoNomina.objects.none()          

            # Renderiza la página de nuevo con el formulario y los mensajes
            contexto = {
                'form': form, 
                'anio': anio, 
                'objetos': objetos,
                'conceptos': conceptos,    
                'role_name': request.role
                }
            return render(request, self.template_name, contexto)


        




class PeriodoDetail (LoginRequiredMixin, View):
    template_name = "app/periodo/detail.html"
    

    def get(self, request, pk):
        registro = get_object_or_404(Periodo, id=pk)
        form = PeriodoForm(instance=registro) 
                     
        anio = registro.anio
        mes = registro.mes
        
        objetos = ObjetoGasto.objects.all()
        conceptos = ConceptoNomina.objects.all()            
        
               
        contexto = { 
            'form': form, 
            'registro': registro,
            'anio': anio, 
            'mes': mes,  
            'objetos': objetos,
            'conceptos': conceptos,              
            'role_name': request.role
            }         
        
        return render(request, self.template_name, contexto )    
    








class PeriodoEdit (LoginRequiredMixin, View):
    template_name = "app/periodo/edit.html"
       
    
    
    def get(self, request, pk):
        registro = get_object_or_404(Periodo, id=pk)
        form = PeriodoForm(instance=registro)                                    
        anio = registro.anio                 
               
        contexto = { 
                    'form': form, 
                    'registro': registro,
                    'anio': anio,  
                    'role_name': request.role
                    }        
        
        return render(request, self.template_name, contexto )    
            
                       
    
    def post(self, request, pk):
        # Obtener el objeto Periodo a editar
        registro = get_object_or_404(Periodo, id=pk)
        
        
       
        # Imprimir todos los campos del registro
        for field in registro._meta.get_fields():
            value = getattr(registro, field.name)
            print(f'{field.name}: {value}')
        
        
        # Obtener el valor de 'obs' desde request.POST
        obs_value = request.POST.get('obs', '')
        
        
        # Ajustar los datos del formulario
        post_data = request.POST.copy()
        if 'anio' not in post_data:
            post_data['anio'] = registro.anio
        if 'mes' not in post_data:
            post_data['mes'] = registro.mes            
        if 'numero' not in post_data:
            post_data['numero'] = registro.numero
        if 'objeto' not in post_data:
            post_data['objeto'] = registro.objeto
        if 'concepto' not in post_data:
            post_data['concepto'] = registro.concepto        
        if 'cerrado' not in post_data:
            post_data['cerrado'] = registro.cerrado      
        if 'tipo_funci' not in post_data:
            post_data['tipo_funci'] = registro.tipo_funci               
                        
            
        # Crear el formulario con los datos POST y el objeto existente        
        form = PeriodoForm(post_data, instance=registro)      
                

        if form.is_valid():
            

            registro = form.save(commit=False)
            registro.obs = obs_value
            registro.save()
            
            messages.success(request, 'El registro se ha editado correctamente.')            
            
            # Redirigir a la lista
            anio = registro.anio
            return redirect('periodo:list', anio=anio)
        
        else:
            # Si el formulario no es válido, mostrar los errores
            errors = form.errors.as_data()
            message = 'Hubo un error al editar el registro. Por favor, verifica los datos:'
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    message += f'\n{field.capitalize()}: {field_error.message}'
            messages.error(request, message)
            
            # Volver a renderizar el formulario con los errores
            return render(request, self.template_name, {'form': form, 'registro': registro})
        
        
        
        
            
    
    
    
class PeriodoDelete (LoginRequiredMixin, View):
    
    def get(self, request, pk):
        remuneracion = get_object_or_404(Periodo, pk=pk)
        anio = remuneracion.anio
        mes = remuneracion.mes
        remuneracion.delete()

        # Agrega un mensaje de éxito
        messages.success(request, 'El registro ha sido eliminado correctamente.')

        # Utiliza reverse para obtener la URL de la vista con los argumentos        
        url = reverse('periodo:list', args=(anio,))

        # Redirige a la URL obtenida
        return redirect(url)




