from django.db import connection
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from django.urls import reverse
from app.models.concepto_nomina_model import ConceptoNomina
from app.models.objeto_gasto_model import ObjetoGasto
from app.models.periodo_model import Periodo
from app.models.proceso_model import Proceso
from config import settings
from datetime import datetime as dt


from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q


from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger






class BonificacionPermanentePagadaList (LoginRequiredMixin, View ):
    
    template_name = 'app/bonificacion_permanente_pagada/list.html'
    items_por_pagina = settings.ITEMS_POR_PAGINA

    def get(self, request, periodo=None ):
        

        periodo_seleccionado = None
        estado_periodo = None
        
        if periodo:
            try:
                periodo_seleccionado = Periodo.objects.get(id=periodo)               
                        
                if periodo_seleccionado.cerrado is not None:
                    estado_periodo = "Abierto" if periodo_seleccionado.cerrado == 0 else "Cerrado"                  
                
            except Periodo.DoesNotExist:
                pass  


        
        
        if periodo_seleccionado:            
            mes = periodo_seleccionado.mes
            anio = periodo_seleccionado.anio
            obj = periodo_seleccionado.objeto.codigo_objeto_gasto
            concepto_id = periodo_seleccionado.concepto.codigo_concepto_nomina
        else:            
            mes = 1  # Asignar un mes predeterminado
            anio = dt.now().year  # Asignar el año actual
            obj = None
            concepto_id = None
            
            
            
            
            

        objetos = ObjetoGasto.objects.filter(codigo_objeto_gasto__in=[113, 133, 199])        
        #conceptos = ConceptoNomina.objects.filter(codigo_concepto_nomina=concepto_id)
        conceptos = ConceptoNomina.objects.filter(og=obj)

                        
        if estado_periodo == "Abierto":
            # Cargar datos de la primera tabla            
            lista_detalle = self.obtener_lista_liquidacion_abierta(periodo_seleccionado)
            print("Periodo Abierto")
            procesos = Proceso.objects.filter(proceso__in=[2])
                    
            
        elif estado_periodo == "Cerrado":
            # Cargar datos de la segunda tabla            
            lista_detalle = self.obtener_lista_liquidacion_cerrada(periodo_seleccionado)
            print("Periodo Cerrado")
            procesos = Proceso.objects.filter(proceso__in=[3])
            
        else:
            # Si el estado es None, enviar una lista vacía
            lista_detalle = []
            procesos = []
        

        # Obtener todos los procesos para el combo select
        



        contexto = {            
            'anio': anio,
            'mes': mes,
            'role_name': request.role,            
            'obj': obj,
            'concepto_id': concepto_id,
            'objetos': objetos,
            'conceptos': conceptos,    
            'periodo_seleccionado': periodo_seleccionado,
            'estado_periodo': estado_periodo, 
            'lista': lista_detalle,
            'procesos': procesos
        }
                        
                        
        return render(request, self.template_name, contexto)
    
    
    
    
    
    
    def obtener_lista_liquidacion_abierta(self, periodo_seleccionado):
        if not periodo_seleccionado:
            return []
        
        query = '''

            SELECT cedula, 
                nombre, devengado, jubilacion, 
                liquido, beneficios_estados.descripcion estado,
                presupuesto, monto113, calculo, porcentaje,
                expediente
            FROM bonificaciones_permanentes
            JOIN beneficios_estados
            ON bonificaciones_permanentes.estado = beneficios_estados.estado
            WHERE anio = %s
            AND mes = %s
            AND objeto = %s
            AND concepto = %s
            AND NOT EXISTS (
                SELECT 1
                FROM bonificaciones_permanentes_pagadas
                WHERE bonificaciones_permanentes_pagadas.id_bonificacion = bonificaciones_permanentes.id
            )
            
        '''
        
        parametros = [
            periodo_seleccionado.anio,
            periodo_seleccionado.mes,
            periodo_seleccionado.objeto.codigo_objeto_gasto,
            periodo_seleccionado.concepto.codigo_concepto_nomina
        ]


        with connection.cursor() as cursor:

            cursor.execute(query, parametros)
            rows = cursor.fetchall()  # Obtener resultados como tuplas
            

        # Convertir tuplas a diccionarios, incluyendo todos los campos
        lista_detalle = [
            {
                'cedula': row[0],
                'nombre': row[1],
                'devengado': row[2],
                'jubilacion': row[3],
                'liquido': row[4],
                'estado': row[5],
                'presupuesto': row[6],
                'monto113': row[7],
                'calculo': row[8],
                'porcentaje': row[9],
                'expediente': row[10],
            } for row in rows
        ]
            
        return lista_detalle

        

    # Método para obtener datos en estado "Cerrado"
    def obtener_lista_liquidacion_cerrada(self, periodo_seleccionado):
        if not periodo_seleccionado:
            return []
        
        query = '''

            SELECT cedula, nombre, pagadas.devengado, jubilacion, liquido, 
            'Pagado' as estado,
            presupuesto, monto113, calculo, porcentaje, imputacion,
            expediente
            FROM bonificaciones_permanentes, bonificaciones_permanentes_pagadas pagadas
            where bonificaciones_permanentes.id =  pagadas.id_bonificacion
            and pagadas.id_periodo = %s
            order by cedula 
            
        '''
        
        parametros = [
            periodo_seleccionado.id
        ]

        with connection.cursor() as cursor:
            cursor.execute(query, parametros)
            rows = cursor.fetchall()  # Obtener resultados como tuplas
            
        # Convertir tuplas a diccionarios, incluyendo todos los campos
        lista_detalle = [
            {
                'cedula': row[0],
                'nombre': row[1],
                'devengado': row[2],
                'jubilacion': row[3],
                'liquido': row[4],
                'estado': row[5],
                'presupuesto': row[6],
                'monto113': row[7],
                'calculo': row[8],
                'porcentaje': row[9],                
                'imputacion': row[10],               
                'expediente': row[11],                                
            } for row in rows
        ]
            
        return lista_detalle        
        
                
        