import os
from django.views import View
from numpy import dtype
from app.forms.marcacion_detalle_forms import MarcacionDetalleForm
from app.models.marcacion_detalle_model import MarcacionDetalle
from config import settings
from datetime import datetime as dt
from django.utils import timezone

from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.contrib import messages
from django.urls import reverse



from utils.beneficios_excel import procesar_beneficios_excel





class  MarcacionDetalleList (LoginRequiredMixin, View ):

    template_name = 'app/marcacion_detalle/list.html'
    items_por_pagina = settings.ITEMS_POR_PAGINA

    def get(self, request, anio=None, mes=None):
        # Obtener el año actual
        anio_actual = dt.now().year

        # Asignar valores por defecto
        mes = mes or 1
        anio = anio or anio_actual
        marcaciones = MarcacionDetalle.objects.filter(anio=anio, mes=mes)

        # Obtener el valor del parámetro 'q' de la URL
        query_param = request.GET.get('q', '')

        # Si hay un valor en el parámetro 'q', realizar la búsqueda en cedula o nombre
        if query_param:
            marcaciones = marcaciones.filter(
                Q(codigo_trabajador__icontains=query_param) |
                Q(nombre_trabajador__icontains=query_param)
            )

        # Paginación
        paginator = Paginator(marcaciones, self.items_por_pagina)
        page = request.GET.get('page')

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            # Si el parámetro de página no es un entero, mostrar la primera página
            lista = paginator.page(1)
        except EmptyPage:
            # Si la página está fuera de rango, mostrar la última página
            lista = paginator.page(paginator.num_pages)

        # Pasar la lista de marcaciones paginada al contexto
        contexto = {'lista': lista, 'anio': anio, 'mes': mes, 'role_name': request.role}

        return render(request, self.template_name, contexto)
    







class MarcacionDetalleDetail (LoginRequiredMixin, View):    
    template_name = 'app/marcacion_detalle/detail.html'

    def get(self, request, pk):
        registro = get_object_or_404(MarcacionDetalle, id=pk)
        form = MarcacionDetalleForm(instance=registro) 
               
        mes = registro.mes
        anio = registro.anio
               
        contexto = { 'form': form, 'registro': registro,
                    'anio': anio, 'mes': mes, 'role_name': request.role}        
        
        return render(request, self.template_name, contexto )    
    







class MarcacionDetalleCreate (LoginRequiredMixin, View):    
    template_name = 'app/marcacion_detalle/create.html'
    
    def get(self, request, *args, **kwargs):  
              
        anio = self.kwargs.get('anio')
        mes = self.kwargs.get('mes')
                
        form = MarcacionDetalleForm()                
        contexto = { 'form': form, 'anio': anio, 'mes': mes, 'role_name': request.role}                        
        return render(request, self.template_name, contexto )
    
    
    def post(self, request, anio, mes):        

        form = MarcacionDetalleForm(request.POST)
                
        if form.is_valid():            
            registro = form.save(commit=False)
            registro.anio = anio
            registro.mes = mes
            
            
            # auditoria de regstro de insert
            registro.usuario_agrega = request.user.id                    
            registro.save()
            
            message = 'El registro se ha agregado correctamente.'
            messages.success(request, message )

        else:
            # Si el formulario no es válido, accede a los errores y envía un mensaje de error detallado
            errors = form.errors.as_data()
            message = 'Hubo un error al agregar el registro. Por favor, verifica los datos:'
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    error_message += f'\n{field.capitalize()}: {field_error.message}'            
            messages.error(request, message)

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {'form': form, 'anio': anio, 'mes': mes, 'role_name': request.role}
        return render(request, self.template_name, contexto)






class MarcacionDetalleEdit (LoginRequiredMixin, View):    
    template_name = 'app/marcacion_detalle/edit.html'
    
    def get(self, request, pk):
        registro = get_object_or_404(MarcacionDetalle, id=pk)
        form = MarcacionDetalleForm(instance=registro) 
               
        mes = registro.mes
        anio = registro.anio                 
               
        contexto = { 'form': form, 'registro': registro,
                    'anio': anio, 'mes': mes, 'role_name': request.role}        
        return render(request, self.template_name, contexto )    
            
            
    
    def post(self, request, pk):        
        
        registro = get_object_or_404(MarcacionDetalle, id=pk)                
        form = MarcacionDetalleForm(request.POST, instance=registro)

        mes = registro.mes
        anio = registro.anio

        if form.is_valid():
            registro = form.save(commit=False)   
            registro.anio = anio
            registro.mes = mes       
            
            # auditoria de regstro para update
            #registro.usuario_agrega = request.user.id     
            registro.usuario_edita = request.user.id                                
            registro.fecha_edita = timezone.now()
                                      
            registro.save()

            message = 'El registro se ha editado correctamente.'
            messages.success(request, message)
        else:
            # Si el formulario no es válido, accede a los errores y envía un mensaje de error detallado
            errors = form.errors.as_data()
            message = 'Hubo un error al editar el registro. Por favor, verifica los datos:'
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    message += f'\n{field.capitalize()}: {field_error.message}'
            messages.error(request, message)

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {'form': form, 'registro': registro, 
                    'anio': anio, 'mes': mes, 'role_name': request.role}
        return render(request, self.template_name, contexto)        
    
    
    
    
    

class MarcacionDetalleDelete(LoginRequiredMixin, View):
    
    def get(self, request, pk):
        registro = get_object_or_404(MarcacionDetalle, pk=pk)
        anio = registro.anio
        mes = registro.mes
        registro.delete()

        # Agrega un mensaje de éxito
        messages.success(request, 'El registro ha sido eliminado correctamente.')

        # Utiliza reverse para obtener la URL de la vista con los argumentos
        url = reverse('marcacion_detalle:list', args=(anio, mes))

        # Redirige a la URL obtenida
        return redirect(url)

    






class MarcacionMigrarArchivo(LoginRequiredMixin, View):    
    template_name = 'app/marcacion_detalle/subirarchivo.html'
    

    def get(self, request):
        context = {
            'role_name': request.role
        }        
        return render(request, self.template_name, context)

    def post(self, request):
        
        if 'excel_file' in request.FILES:
            excel_file = request.FILES['excel_file']
            anio = request.POST.get('anio')
            mes = request.POST.get('mes')

            # Garantizar que la carpeta "uploads" exista
            destination_folder = os.path.join(settings.MEDIA_ROOT, 'uploads')
            os.makedirs(destination_folder, exist_ok=True)

            # Guardar el archivo en el servidor
            save_path = os.path.join(destination_folder, excel_file.name)
            with open(save_path, 'wb') as destination:
                for chunk in excel_file.chunks():
                    destination.write(chunk)

            # Procesar el archivo utilizando la función compartida
            exito = procesar_beneficios_excel(save_path, anio, mes)

            # Verificar si la operación fue exitosa utilizando el valor devuelto por la función
            if exito:
                # Si la operación fue exitosa
                messages.success(request, 'Migracion completa')
            else:
                # Si la operación no fue exitosa
                messages.error(request, 'Error en el procesamiento del archivo.')


        context = {
            'role_name': request.role
        }        


        return render(request, self.template_name, context)            
            
        
