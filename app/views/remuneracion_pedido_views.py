import json
import os
from datetime import datetime as dt
import traceback
from django.db import connections
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views import View
from django.db.models import Q
from django.contrib import messages
from django.urls import reverse
from django.utils import timezone
from app.forms.remuneracion_pedido_forms import RemuneracionesPedidosForm
from app.models.remuneracion_pedido_model import RemuneracionPedido
from config import settings
from django.contrib.auth.mixins import LoginRequiredMixin





class RemuneracionesPedidosLista(LoginRequiredMixin, View ):
    
    template_name = 'app/remuneracion_pedido/list.html'
    
    items_por_pagina = 20  # Puedes ajustar este valor según tus necesidades

    def get(self, request, anio=None, mes=None):
        # Obtener el año actual
        anio_actual = dt.now().year

        # Asignar valores por defecto
        mes = mes or 1
        anio = anio or anio_actual
        coleccion = RemuneracionPedido.objects.filter(anio=anio, mes=mes).order_by('cedula')

        # Obtener el valor del parámetro 'q' de la URL
        query_param = request.GET.get('q', '')

        # Si hay un valor en el parámetro 'q', realizar la búsqueda en cedula o nombre
        if query_param:
            coleccion = coleccion.filter(
                Q(cedula__icontains=query_param) |
                Q(nombre__icontains=query_param)
            )
        # Calcula el conteo de elementos de la consulta
        cantidad_registros = coleccion.count()            
           

        # Paginación
        paginator = Paginator(coleccion, self.items_por_pagina)
        page = request.GET.get('page')

        try:
            lista = paginator.page(page)
        except PageNotAnInteger:
            # Si el parámetro de página no es un entero, mostrar la primera página
            lista = paginator.page(1)
        except EmptyPage:
            # Si la página está fuera de rango, mostrar la última página
            lista = paginator.page(paginator.num_pages)


        # Imprimir mensaje en la consola
        print("-- cantidad     ", cantidad_registros)

        # Pasar la lista de remuneraciones paginada al contexto
        contexto = {'lista': lista, 'anio': anio, 'mes': mes, 'role_name': request.role,
                    'cantidad_registros': cantidad_registros} 
                
        return render(request, self.template_name, contexto)
    








class RemuneracionPedidoCrear(LoginRequiredMixin, View):
    
    template_name = 'app/remuneracion_pedido/create.html'    
    
    def get(self, request, *args, **kwargs):  
              
        anio = self.kwargs.get('anio')
        mes = self.kwargs.get('mes')
                
        form = RemuneracionesPedidosForm()                
        contexto = { 'form': form, 'anio': anio, 'mes': mes, 'role_name': request.role}                        
        return render(request, self.template_name, contexto )
    
    

    def post(self, request, anio, mes):        

        form = RemuneracionesPedidosForm(request.POST)
        
        if form.is_valid():        
                
            registro = form.save(commit=False)
            registro.anio = anio
            registro.mes = mes
            
            
            # auditoria de regstro de insert
            registro.usuario = request.user.id                    
            
            registro.nombre = "fdsfsfdsdfafsaf"     
            
            registro.save()
            
            message = 'El registro se ha agregado correctamente.'
            messages.success(request, message )

        else:
            # Si el formulario no es válido, accede a los errores y envía un mensaje de error detallado
            errors = form.errors.as_data()
            error_message = 'Hubo un error al agregar el registro. Por favor, verifica los datos:'
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    error_message += f'\n{field.capitalize()}: {field_error.message}'            
            messages.error(request, message)

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {'form': form, 'anio': anio, 'mes': mes, 'role_name': request.role}
        return render(request, self.template_name, contexto)


      



class RemuneracionPedidoEditar(LoginRequiredMixin, View):
    
    template_name = 'app/remuneracion_pedido/edit.html'    
    
    
    def get(self, request, pk):
        registro = get_object_or_404(RemuneracionPedido, id=pk)
        form = RemuneracionesPedidosForm(instance=registro) 
               
        mes = registro.mes
        anio = registro.anio                 
               
        contexto = { 'form': form, 'registro': registro,
                    'anio': anio, 'mes': mes, 'role_name': request.role}        
        return render(request, self.template_name, contexto )    
            
            
            
    
    def post(self, request, pk):        
        registro = get_object_or_404(RemuneracionPedido, id=pk)                
        form = RemuneracionesPedidosForm(request.POST, instance=registro)
        

        mes = registro.mes
        anio = registro.anio


        if form.is_valid():
            
            registro = form.save(commit=False)   
            registro.anio = anio            
            registro.mes = mes  
            
            
            
            registro.save()

            message = 'El registro se ha editado correctamente.'
            messages.success(request, message)
        else:
            # Si el formulario no es válido, accede a los errores y envía un mensaje de error detallado
            errors = form.errors.as_data()
            message = 'Hubo un error al editar el registro. Por favor, verifica los datos:'
            for field, field_errors in errors.items():
                for field_error in field_errors:
                    message += f'\n{field.capitalize()}: {field_error.message}'
            messages.error(request, message)

        # Renderiza la página de nuevo con el formulario y los mensajes
        contexto = {'form': form, 'registro': registro, 
                    'anio': anio, 'mes': mes, 'role_name': request.role}
        
        return render(request, self.template_name, contexto)        
    
    
    
    
    
    
    
    
    
class RemuneracionPedidoBorrar(LoginRequiredMixin, View):
    
    def get(self, request, pk):
        remuneracion = get_object_or_404(RemuneracionPedido, pk=pk)
        anio = remuneracion.anio
        mes = remuneracion.mes
        remuneracion.delete()

        # Agrega un mensaje de éxito
        messages.success(request, 'El registro ha sido eliminado correctamente.')

        # Utiliza reverse para obtener la URL de la vista con los argumentos
        url = reverse('remuneraciones_pedidos:lista', args=(anio, mes))

        # Redirige a la URL obtenida
        return redirect(url)








class RemuneracionPedidoDetalles(LoginRequiredMixin, View):
    template_name = 'app/remuneracion_pedido/detail.html'

    def get(self, request, pk):
        remuneracion = get_object_or_404(RemuneracionPedido, id=pk)
        form = RemuneracionesPedidosForm(instance=remuneracion) 
               
        mes = remuneracion.mes
        anio = remuneracion.anio
               
        contexto = { 'form': form, 'remuneracion': remuneracion,
                    'anio': anio, 'mes': mes, 'role_name': request.role}        
        
        return render(request, self.template_name, contexto )    
    






class RemuneracionPedidoReplicarLista(LoginRequiredMixin, View):
      
    def post(self, request, *args, **kwargs):
        # Obtener los datos enviados desde JavaScript
        datos_recibidos = json.loads(request.body)
        
        # Acceder a los valores de los datos recibidos        
        anio = int(datos_recibidos.get('anio'))
        mes = int(datos_recibidos.get('mes'))
        
        # Imprimir los valores recibidos en la consola
        print("Año:", anio)
        print("Mes:", mes)
        
        
        try:
        
            if mes == 1:
                anio_anterior = anio - 1
                mes_anterior = 12
            else:
                anio_anterior = anio
                mes_anterior = mes - 1

            
            sql = f"""
            INSERT INTO remuneraciones_pedidos ( 
                anio, mes, cedula, nombre, objeto, id_depende, horas, horas_ra,
                expediente, memorandum, actualizo, observacion, usuario, cod_og, encontro           
            )
            SELECT {anio}, {mes}, cedula, nombre, objeto, id_depende, horas, horas_ra,
                expediente, memorandum, actualizo, observacion, usuario, cod_og, encontro            
            FROM remuneraciones_pedidos
            WHERE anio = {anio_anterior} AND mes = {mes_anterior}
            """        
            
            
            # Ejecutar el SQL utilizando la configuración de la base de datos
            with connections['default'].cursor() as cursor:
                cursor.execute(sql)
                connections['default'].commit()
                    
            # Registro de éxito
            print("Los datos del mes anterior se han copiado exitosamente.")                    
                    
            
            # Si llegamos aquí sin lanzar una excepción, la operación fue exitosa
            respuesta = {"mensaje": "Los datos del mes anterior se han copiado exitosamente."}
            return JsonResponse(respuesta)
        
        except Exception as e:
            
            # Registro de error
            print(f"Hubo un problema al copiar los datos del mes anterior: {str(e)}")            
            
            # Si ocurre alguna excepción, manejarla y devolver un mensaje de error
            respuesta = {"error": f"Hubo un problema al copiar los datos del mes anterior: {str(e)}"}
            return JsonResponse(respuesta, status=500)     


  
  
  
  
  
  
  
  
  
  