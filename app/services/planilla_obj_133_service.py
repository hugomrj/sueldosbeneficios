import json
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render

from django.urls import reverse
from app.models.beneficio_estado import BeneficioEstado
from app.models.bonificacion_pedido_model import BonificacionPedido
from app.models.bonificacion_permanente_model import BonificacionPermanente
from app.models.cargo_model import Cargo
from app.models.tipo_cargo_model import TipoCargo
from app.models.trabajador_model import Trabajador

from datetime import datetime as dt
from django.core.exceptions import ObjectDoesNotExist

from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin





def generar_planilla_obj_133_1133010(request, anio, mes, objeto, concepto, pedidos_a_procesar=None):
    
    
    if pedidos_a_procesar is not None:
        # Usar directamente los pedidos que ya fueron filtrados
        bonificaciones_pedidos = pedidos_a_procesar
    else:
        # La lógica original como respaldo
        bonificaciones_pedidos = BonificacionPedido.objects.filter(
            objeto=objeto, 
            concepto=concepto,
            estado__estado=1
            )
            
    numero_de_registros = bonificaciones_pedidos.count()



    if numero_de_registros == 0:        
        raise ValueError("No se encontraron registros de solicitudes para la bonificación.")


    for bonificacion in bonificaciones_pedidos:
        cedula = bonificacion.cedula
        


        # Realizar la consulta SQL sobre trabajador
        try:
            trabajador = Trabajador.objects.get(cedula=cedula)
        except Trabajador.DoesNotExist:
            raise ValueError("No existe trabajador con cédula {}".format(cedula))
        

        # Construir la consulta SQL con los parámetros
        query = """
            SELECT bonificaciones_permanentes_pagadas.devengado
            FROM bonificaciones_permanentes
            INNER JOIN bonificaciones_permanentes_pagadas 
                ON bonificaciones_permanentes.id = bonificaciones_permanentes_pagadas.id_bonificacion
            WHERE cedula = %s
            AND objeto = 113
            AND anio = %s
            AND mes = %s
        """

        # Ejecutar la consulta con los parámetros
        with connection.cursor() as cursor:
            cursor.execute(query, [cedula, anio, mes])

            # Obtiene el resultado de la consulta
            result = cursor.fetchone()


        print("-- hasta aca 2")

        # Si se encuentra un resultado, asigna el valor a monto113, si no, asigna 0
        if result:
            monto113 = result[0]  # El resultado debe ser un solo valor
        else:
            monto113 = 0

        print("-- hasta aca 3")        

        asignacion = bonificacion.presupuestado

        print("-- hasta aca 4")   

        print(f"-- hasta aca 4: asignacion={asignacion}, monto113={monto113}")
        
        calculo = asignacion + monto113

        print(f"Resultado del cálculo: {calculo}")

        monto_porcentaje = (calculo * bonificacion.porcentaje) / 100


        print("-- hasta aca 5")        



        # Verificar si está exenta de jubilación
        jubilacion = None
        query = """
            SELECT cedula
            FROM ley2345
            WHERE cedula = %s
        """
        params = [bonificacion.cedula]

        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()

        print("-- hasta aca 6")


        if results:
            jubilacion = 0
        else:
            jubilacion = monto_porcentaje * 0.16
            

        estado_activo = get_object_or_404(BeneficioEstado, estado='1')


        # Insertar registro en BonificacionPermanente
        BonificacionPermanente.objects.create(
            anio=anio,
            mes=mes,
            objeto=bonificacion.objeto,
            concepto=bonificacion.concepto,
            subconcepto=bonificacion.subconcepto,
            actividad=1,              
            cedula=bonificacion.cedula,
            nombre=trabajador.nombre,
            
            presupuesto=asignacion,
            
            porcentaje=bonificacion.porcentaje,
            devengado = asignacion,
            jubilacion=jubilacion,            
            ccargo=bonificacion.ccargo,
            tipo_cargo=None,            
            cinsti= bonificacion.cinsti,            
            imputacion=bonificacion.imputacion, 
            id_pedido = bonificacion.id,                   
            estado=estado_activo,
            monto113 = monto113,
            calculo=monto_porcentaje,
            liquido=monto_porcentaje-jubilacion,            
        )        

      


    return True
    







def generar_planilla_obj_133_1133030(request, anio, mes, objeto, concepto, pedidos_a_procesar=None):
    
    if pedidos_a_procesar is not None:
        # Usar directamente los pedidos que ya fueron filtrados
        bonificaciones_pedidos = pedidos_a_procesar
    else:
        # La lógica original como respaldo
        bonificaciones_pedidos = BonificacionPedido.objects.filter(
            objeto=objeto, 
            concepto=concepto,
            estado__estado=1
            )
            
    numero_de_registros = bonificaciones_pedidos.count()
    
    
    if numero_de_registros == 0:        
        raise ValueError("No se encontraron registros de solicitudes para la bonificación.")


    for bonificacion in bonificaciones_pedidos:
        cedula = bonificacion.cedula
        

        # Realizar la consulta SQL sobre trabajador
        try:
            trabajador = Trabajador.objects.get(cedula=cedula)
        except Trabajador.DoesNotExist:
            raise ValueError("No existe trabajador con cédula {}".format(cedula))
        


        # Construir la consulta de sueldos actual
        query = """

            SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
            FROM sueldo_actual
            where cedula_id = %s

        """

        # Ejecutar la consulta con los parámetros
        with connection.cursor() as cursor:
            cursor.execute(query, [cedula])
            # Obtiene todos los resultados de la consulta
            results = cursor.fetchall()
            # Cuenta la cantidad de registros
            total_registros = len(results)

        # Ahora puedes usar `total_registros`
        print(f"Total de registros: {total_registros}")

        presupuestado = 0
        monto_tope = 7300000
        devengado = 0
        calculo = 0

        if total_registros == 1:
            # Si hay exactamente un registro
            registro = results[0]  
            presupuestado = registro[3]  # PRESUP_ACT está en la cuarta columna (índice 3)

            
        else:

            # Si hay más de un registro
            suma_presupuesto = 0

            # tiene un administrativo             
            query = """
                SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
                FROM sueldosbeneficios.sueldo_actual
                WHERE cedula_id = %s
                AND CATEGO_PSP NOT REGEXP '^[LZ]';
            """
            # Ejecutar la consulta con los parámetros
            with connection.cursor() as cursor:
                cursor.execute(query, [cedula])
                results = cursor.fetchall()
                total_registros = len(results)

            # Verificar si existen registros
            if total_registros == 1:
                # se obtiene el rubro administrativo
                registro = results[0]  
                presupuestado = registro[3] 
                
            else:
                # Si tiene solo L's y Z's
                query = """
                    SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
                    FROM sueldosbeneficios.sueldo_actual
                    WHERE cedula_id = %s
                    AND CATEGO_PSP REGEXP '^[LZ]'
                    order by CATEGO_PSP, PRESUP_ACT;
                """


                # Ejecutar la consulta
                with connection.cursor() as cursor:
                    cursor.execute(query, [cedula])
                    registros = cursor.fetchall()  # Recuperar todos los registros

                # Verificar la condición
                if registros:
                    # Obtener categorías únicas para evaluar las condiciones
                    categorias = {registro[1][0] for registro in registros}  # Tomar solo la primera letra de CATEGO_PSP

                    if "L" in categorias:
                        # Aquí haces lo que necesites cuando hay al menos un "L"
                        print("Tiene al menos un registro con L. Realizar acción específica.")
                        presupuestado = registros[0][3] 

                    elif categorias == {"Z"}:
                        print("Tiene solo registros con Z. Realizar otra acción.")
                        # Aquí haces lo que necesites cuando hay solo "Z"


                    else:
                        print("La condición no se cumple específicamente para L o solo Z.")




        if presupuestado > monto_tope:
            calculo = monto_tope  # Si presupuestado excede el tope, calculo es igual al tope
        else:
            calculo = presupuestado  # Si no, calculo es igual a presupuestado

        # Devengado es el 30% del cálculo
        devengado = calculo * 0.3   


        # Verificar si está exenta de jubilación
        jubilacion = None
        query = """
            SELECT cedula
            FROM ley2345
            WHERE cedula = %s
        """
        params = [bonificacion.cedula]

        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()

        if results:
            jubilacion = 0
        else:
            jubilacion = devengado * 0.16
            

        estado_activo = get_object_or_404(BeneficioEstado, estado='1')


        # Insertar registro en BonificacionPermanente
        BonificacionPermanente.objects.create(
            anio=anio,
            mes=mes,
            objeto=bonificacion.objeto,
            concepto=bonificacion.concepto,
            subconcepto=bonificacion.subconcepto,
            actividad=1,              
            cedula=bonificacion.cedula,
            nombre=trabajador.nombre,
            
            presupuesto=presupuestado,
            
            porcentaje=bonificacion.porcentaje,
            devengado = devengado,
            
            jubilacion=jubilacion,            
            ccargo=bonificacion.ccargo,
            tipo_cargo=None,            
            cinsti= bonificacion.cinsti,            
            imputacion=bonificacion.imputacion, 
            id_pedido = bonificacion.id,                   
            estado=estado_activo,            
            calculo=calculo,
            liquido=devengado-jubilacion, 
            expediente=bonificacion.expediente
        )        





    return True
    







def generar_planilla_obj_133_1133040(request, anio, mes, objeto, concepto, pedidos_a_procesar=None):
    
    
    if pedidos_a_procesar is not None:
        # Usar directamente los pedidos que ya fueron filtrados
        bonificaciones_pedidos = pedidos_a_procesar
    else:
        # La lógica original como respaldo
        bonificaciones_pedidos = BonificacionPedido.objects.filter(
            objeto=objeto, 
            concepto=concepto,
            estado__estado=1
            )
            
    numero_de_registros = bonificaciones_pedidos.count()


    if numero_de_registros == 0:        
        raise ValueError("No se encontraron registros de solicitudes para la bonificación.")


    for bonificacion in bonificaciones_pedidos:
        cedula = bonificacion.cedula
        

        # Realizar la consulta SQL sobre trabajador
        try:
            trabajador = Trabajador.objects.get(cedula=cedula)
        except Trabajador.DoesNotExist:
            raise ValueError("No existe trabajador con cédula {}".format(cedula))
        


        # Construir la consulta de sueldos actual
        query = """

            SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
            FROM sueldo_actual
            where cedula_id = %s

        """

        # Ejecutar la consulta con los parámetros
        with connection.cursor() as cursor:
            cursor.execute(query, [cedula])
            # Obtiene todos los resultados de la consulta
            results = cursor.fetchall()
            # Cuenta la cantidad de registros
            total_registros = len(results)

        # Ahora puedes usar `total_registros`
        print(f"Total de registros: {total_registros}")

        presupuestado = 0
        monto_tope = 7300000
        devengado = 0
        calculo = 0

        if total_registros == 1:
            # Si hay exactamente un registro
            registro = results[0]  
            presupuestado = registro[3]  # PRESUP_ACT está en la cuarta columna (índice 3)

            
        else:

            # Si hay más de un registro
            suma_presupuesto = 0

            # tiene un administrativo             
            query = """
                SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
                FROM sueldosbeneficios.sueldo_actual
                WHERE cedula_id = %s
                AND CATEGO_PSP NOT REGEXP '^[LZ]';
            """
            # Ejecutar la consulta con los parámetros
            with connection.cursor() as cursor:
                cursor.execute(query, [cedula])
                results = cursor.fetchall()
                total_registros = len(results)

            # Verificar si existen registros
            if total_registros == 1:
                # se obtiene el rubro administrativo
                registro = results[0]  
                presupuestado = registro[3] 
                
            else:
                # Si tiene solo L's y Z's
                query = """
                    SELECT cedula_id, CATEGO_PSP, CANT_RUBRO, PRESUP_ACT
                    FROM sueldosbeneficios.sueldo_actual
                    WHERE cedula_id = %s
                    AND CATEGO_PSP REGEXP '^[LZ]'
                    order by CATEGO_PSP, PRESUP_ACT;
                """


                # Ejecutar la consulta
                with connection.cursor() as cursor:
                    cursor.execute(query, [cedula])
                    registros = cursor.fetchall()  # Recuperar todos los registros

                # Verificar la condición
                if registros:
                    # Obtener categorías únicas para evaluar las condiciones
                    categorias = {registro[1][0] for registro in registros}  # Tomar solo la primera letra de CATEGO_PSP

                    if "L" in categorias:
                        # Aquí haces lo que necesites cuando hay al menos un "L"
                        print("Tiene al menos un registro con L. Realizar acción específica.")
                        presupuestado = registros[0][3] 

                    elif categorias == {"Z"}:
                        print("Tiene solo registros con Z. Realizar otra acción.")
                        # Aquí haces lo que necesites cuando hay solo "Z"


                    else:
                        print("La condición no se cumple específicamente para L o solo Z.")




        if presupuestado > monto_tope:
            calculo = monto_tope  # Si presupuestado excede el tope, calculo es igual al tope
        else:
            calculo = presupuestado  # Si no, calculo es igual a presupuestado

        # Devengado es el 30% del cálculo
        devengado = calculo * 0.3   


        # Verificar si está exenta de jubilación
        jubilacion = None
        query = """
            SELECT cedula
            FROM ley2345
            WHERE cedula = %s
        """
        params = [bonificacion.cedula]

        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()

        if results:
            jubilacion = 0
        else:
            jubilacion = devengado * 0.16
            

        estado_activo = get_object_or_404(BeneficioEstado, estado='1')


        # Insertar registro en BonificacionPermanente
        BonificacionPermanente.objects.create(
            anio=anio,
            mes=mes,
            objeto=bonificacion.objeto,
            concepto=bonificacion.concepto,
            subconcepto=bonificacion.subconcepto,
            actividad=1,              
            cedula=bonificacion.cedula,
            nombre=trabajador.nombre,
            
            presupuesto=presupuestado,
            
            porcentaje=bonificacion.porcentaje,
            devengado = devengado,
            
            jubilacion=jubilacion,            
            ccargo=bonificacion.ccargo,
            tipo_cargo=None,            
            cinsti= bonificacion.cinsti,            
            imputacion=bonificacion.imputacion, 
            id_pedido = bonificacion.id,                   
            estado=estado_activo,            
            calculo=calculo,
            liquido=devengado-jubilacion, 
            expediente=bonificacion.expediente
        )        





    return True
    
