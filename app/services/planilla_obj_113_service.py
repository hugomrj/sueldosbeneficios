import json
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render

from django.urls import reverse
from app.models.beneficio_estado import BeneficioEstado
from app.models.bonificacion_pedido_model import BonificacionPedido
from app.models.bonificacion_permanente_model import BonificacionPermanente
from app.models.cargo_model import Cargo
from app.models.tipo_cargo_model import TipoCargo
from app.models.trabajador_model import Trabajador

from datetime import datetime as dt
from django.core.exceptions import ObjectDoesNotExist

from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin










def generar_planilla_obj_113_1113000(request, anio, mes, objeto, concepto,  pedidos_a_procesar=None):
    

    if pedidos_a_procesar is not None:
        # Usar directamente los pedidos que ya fueron filtrados
        bonificaciones_pedidos = pedidos_a_procesar
    else:
        # La lógica original como respaldo
        bonificaciones_pedidos = BonificacionPedido.objects.filter(
            objeto=objeto, 
            concepto=concepto,
            estado__estado=1
            )
            
    numero_de_registros = bonificaciones_pedidos.count()


    if numero_de_registros == 0:        
        raise ValueError("No se encontraron registros de solicitudes para la bonificación.")
    
    
    for bonificacion in bonificaciones_pedidos:
        
        # print(bonificacion.__dict__)
        
        cedula = bonificacion.cedula
        
        
        # Realizar la consulta SQL sobre trabajador
        try:
            trabajador = Trabajador.objects.get(cedula=cedula)
        except Trabajador.DoesNotExist:
            raise ValueError("No existe trabajador con cédula {}".format(cedula))
        
                        
        query = """
            SELECT categoria, ccargo, asignacion, jubilacion
            FROM anexo113
            WHERE id = %s
        """
        params = [bonificacion.cargo113]


        with connection.cursor() as cursor:
            cursor.execute(query, params)
            anexo113 = cursor.fetchone()  

            # Verificar si la consulta devolvió un resultado
            if not anexo113:
                print("No existe relación para cargo {}".format(bonificacion.ccargo.pk))
                raise ValueError("No existe relación para cargo {}".format(bonificacion.ccargo.pk))
            else:
                # Verificar si se devolvieron los 4 valores esperados
                if len(anexo113) == 4:
                    # Desempaquetar los resultados para imprimir correctamente
                    categoria, ccargo, asignacion, jubilacion = anexo113
                    print(f"Existe anexo 113 - Categoría: {categoria}, Cargo: {ccargo}, Asignación: {asignacion}, Jubilación: {jubilacion}")
                else:
                    print(f"Error: Se esperaban 4 columnas, pero se recibieron {len(anexo113)}.")           
    
        
        
        # Verficar si esta exenta de jubilacion
        query = """
            SELECT cedula
            FROM ley2345
            where cedula = %s
        """
        params = [bonificacion.cedula]
        
        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()
        
        if results:
            jubilacion = 0
     
        
        print(bonificacion.id)
        
        estado_activo = get_object_or_404(BeneficioEstado, estado='1')
        
        
        # Insertar registro en BonificacionPermanente
        BonificacionPermanente.objects.create(
            anio=anio,
            mes=mes,
            objeto=bonificacion.objeto,
            concepto=bonificacion.concepto,
            subconcepto=bonificacion.subconcepto,
            actividad=1,              
            cedula=bonificacion.cedula,
            nombre=trabajador.nombre,
            categoria=categoria,
            presupuesto=asignacion,
            calculo=None,  # Asigna el valor adecuado si lo tienes
            porcentaje=bonificacion.porcentaje,
            devengado = asignacion,
            jubilacion=jubilacion,
            liquido=asignacion-jubilacion,            
            ccargo=bonificacion.ccargo,
            tipo_cargo=None,            
            cinsti= bonificacion.cinsti,            
            imputacion=bonificacion.imputacion, 
            id_pedido = bonificacion.id,                   
            estado=estado_activo
        )        


        

    # messages.success(request, "Proceso realizado correctamente. desde final")
    return True




