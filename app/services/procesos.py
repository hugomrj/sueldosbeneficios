import json
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render
from django.db.models import Subquery


from django.urls import reverse
from app.models.beneficio_estado import BeneficioEstado
from app.models.bonificacion_pedido_model import BonificacionPedido
from app.models.bonificacion_permanente_model import BonificacionPermanente


from app.models.bonificacion_permanente_pagado_model import BonificacionesPermanentesPagadas
from app.models.periodo_model import Periodo
from app.services.planilla_obj_113_service import  generar_planilla_obj_113_1113000

from app.services.planilla_obj_133_service import (
     generar_planilla_obj_133_1133010, generar_planilla_obj_133_1133030, generar_planilla_obj_133_1133040
     )

from config import settings
from datetime import datetime as dt


from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.db.models import Q


from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



# proceso para crear planilla objeto 113, 133
def proceso_numero_1( request, anio, mes, objeto, concepto ):
    
    print("proceso 1")
    
    try:            
        
        
        anio = int(anio)
        mes = int(mes)
        objeto = int(objeto)
                    
        # verificar si existen datos para que hagan falta generar
        # Obtener registros de pedidos que cumplen los criterios
        pedidos_query = BonificacionPedido.objects.filter(
            anio=anio,
            objeto=objeto,
            concepto=concepto,
            estado=1
        )
        
        # Obtener cédulas de permanentes que cumplen los criterios
        permanentes_cedulas = BonificacionPermanente.objects.filter(
            anio=anio,
            mes=mes,
            objeto=objeto,
            concepto=concepto
        ).values_list('cedula', flat=True)
        
        # Filtrar pedidos que no están en permanentes
        pedidos_a_procesar = pedidos_query.exclude(cedula__in=permanentes_cedulas)
                
        # Verificar si hay registros para procesar
        if pedidos_a_procesar.exists():
            # Mostrar mensaje por consola
            print(f"Se encontraron {pedidos_a_procesar.count()} registros para procesar")
            
            # Si quieres ver las cédulas, puedes agregar:
            print("Cédulas a procesar:", list(pedidos_a_procesar.values_list('cedula', flat=True)))

        else:
            # No hay registros que procesar
            print("No se encontraron registros para procesar")
            messages.warning(request, "No se encontraron pedidos para procesar")
            return False


        
        # verifiaca si existe datos de sueldos en el mes y año señeccionado
        with connection.cursor() as cursor:            
            cursor.execute("""
                SELECT COUNT(*) AS cantidad_registros
                FROM sueldosbeneficios.sueldo_actual
                WHERE AA_PLAN = %s AND MM_PLAN = %s
            """, [anio, mes])
            # Obtiene el resultado
            result = cursor.fetchone()
            count = result[0]   
        # Verifica si el conteo es cero
        if count == 0:
            raise ValueError("No datos de planilla sueldos para datos seleccionados")
                                                    
        print("cantidad registros sueldo actual")
        
        # seleccinar obketo ssegun el objeto
        if objeto == 113:                        
            
            if concepto == '1113000':
                print("entro en  1113000 ")
                rotorno = generar_planilla_obj_113_1113000( request, anio, mes, objeto, concepto,
                                                            pedidos_a_procesar )

        if objeto == 133:
            
            rotorno = True    

            if concepto == '1133010':
                print("entro en  1133010 ")
                rotorno = generar_planilla_obj_133_1133010( request, anio, mes, objeto, concepto,
                                                            pedidos_a_procesar )



            elif concepto == '1133030':
                print("entro en  1133030 ")
                rotorno = generar_planilla_obj_133_1133030( request, anio, mes, objeto, concepto,
                                                            pedidos_a_procesar )

            
            elif concepto == '1133040':
                print("entro en  1133040 ")
                rotorno = generar_planilla_obj_133_1133040( request, anio, mes, objeto, concepto,
                                                            pedidos_a_procesar )



            else:
                print("No entró en ninguna de las condiciones de concepto.")
                rotorno = False


            
            

        '''
        elif self.objeto == 133:
            self.generar_planilla_obj133()
        elif self.objeto == 199:
            self.generar_planilla_obj199()
        else:
            print(f"Objeto {self.objeto} no requiere una función específica")            
        '''                

        
        if rotorno:
            messages.success(request, "Proceso realizado correctamente.")
        else:
            messages.error(request, "Ocurrió un error en el proceso.")        
        
        
        
        return rotorno

    
    except ValueError as ve:
        messages.error(request, str(ve))
        return False
    

    except Exception as e:
        messages.error(request, "Ocurrió un error inesperado durante el proceso.")
        print(f"Error: {str(e)}")  # Imprimir el error en la consola para depuración
    











# proceso para generar liquidacion y asociar al periodo
def proceso_numero_2(request, periodo_id):
    
    try:            

        # primero filtarr periodo con el parametro periodo_id
        
        # Filtrar periodo con el parámetro periodo_id
        periodo = Periodo.objects.get(id=periodo_id)  # Busca el periodo por ID
        print(f"Cerrado: {periodo.cerrado}")

        # Verificar si el periodo está cerrado
        if periodo.cerrado == 0:
            
            print("El periodo está abierto. Continuando con el proceso...")
            # Aquí puedes añadir la lógica para generar la liquidación
            
            pagadas_ids = BonificacionesPermanentesPagadas.objects.all().values('id_bonificacion')
            
            
            # Filtrar BonificacionPermanente usando los valores del periodo y estado igual a 1
            bonificaciones = BonificacionPermanente.objects.filter(
                anio=periodo.anio,
                mes=periodo.mes,
                objeto=periodo.objeto,  # Asegúrate de que objeto es del mismo tipo
                concepto=periodo.concepto,  # Asegúrate de que concepto es del mismo tipo
                estado__estado=1  # Filtra por estado igual a 1
            ).exclude(id__in=Subquery(pagadas_ids))

            if not bonificaciones.exists():
                raise ValueError("No se encontraron bonificaciones para el periodo especificado.")



            print("Bonificaciones encontradas:")
            for bonificacion in bonificaciones:
                print(f"ID: {bonificacion.id}, Nombre: {bonificacion.nombre}, Estado: {bonificacion.estado}")

                # Crear e insertar el registro en BonificacionesPermanentesPagadas
                nueva_bonificacion_pagada = BonificacionesPermanentesPagadas(
                    id_periodo=periodo_id,
                    id_bonificacion=bonificacion.id,                    
                    devengado=bonificacion.devengado,
                    id_estado = 1                  
                )

                # Guardar la nueva bonificación pagada
                nueva_bonificacion_pagada.save()
                print(f"Registro insertado: {nueva_bonificacion_pagada.id}")
            
            
            # Después de finalizar el proceso, se cierra el periodo
            periodo.cerrado = 1  # Cambia el estado a cerrado
            periodo.save()  # Guarda los cambios en la base de datos



            # Estado de registro actualizar a pagado
            estado_obj = BeneficioEstado.objects.get(estado=2)
            # Actualizar bonificaciones
            for bonificacion in bonificaciones:
                bonificacion.estado = estado_obj
                bonificacion.save()


            print(f"Periodo {periodo_id} cerrado exitosamente.")            
            
            rotorno = True  # Simulación de éxito
            
            
        else:
            # Generar error si el periodo está cerrado
            messages.error(request, "El periodo está cerrado y no se puede procesar.")
            print("Error: El periodo está cerrado.")
            rotorno = True
        
        
        if rotorno:
            messages.success(request, "Proceso realizado correctamente.")
        
        return rotorno

    
    except ValueError as ve:
        messages.error(request, str(ve))
        return False
    
    except Exception as e:
        messages.error(request, "Ocurrió un error inesperado durante el proceso.")
        print(f"Error: {str(e)}")  # Imprimir el error en la consola para depuración
    









# proceso para generar liquidacion y asociar al periodo
def proceso_numero_3(request, periodo_id):
    
    try:            
        
        # Filtrar periodo con el parámetro periodo_id
        periodo = Periodo.objects.get(id=periodo_id)  # Busca el periodo por ID

        # Verificar si el periodo está cerrado
        if periodo.cerrado == 1:
            
            print("El periodo está abierto. Continuando con el proceso...")
            # Aquí puedes añadir la lógica para generar la liquidación
            

            # Obtener el valor de 'imputacion' del formulario enviado
            imputacion_value = request.POST.get('imputacion', None)
            print(f"Valor de imputación recibido: {imputacion_value}")  # Mostrar en consola


            with connection.cursor() as cursor:
                # Ejecutar la consulta SQL
                cursor.execute("""
                    UPDATE bonificaciones_permanentes
                    SET imputacion = %s
                    WHERE id IN (
                        SELECT id_bonificacion 
                        FROM bonificaciones_permanentes_pagadas
                        WHERE ID_PERIODO = %s
                    )
                """, [imputacion_value, periodo_id])
            
            rotorno = True


        else:
            # Generar error si el periodo está cerrado
            messages.error(request, "El periodo no esta cerrado.")
            rotorno = True
        

        
        if rotorno:
            messages.success(request, "Proceso realizado correctamente.")
        
        return rotorno

    
    except ValueError as ve:
        messages.error(request, str(ve))
        return False
    
    except Exception as e:
        messages.error(request, "Ocurrió un error inesperado durante el proceso.")
        print(f"Error: {str(e)}")  # Imprimir el error en la consola para depuración
    

