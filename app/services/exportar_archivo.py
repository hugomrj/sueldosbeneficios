from django.http import HttpResponse
from openpyxl import Workbook
from io import BytesIO

from app.models.bonificacion_permanente_model import BonificacionPermanente


def exportar_excel(request):
    # Obtén los parámetros de la consulta
    anio = request.GET.get('anio')
    mes = request.GET.get('mes')
    objeto = request.GET.get('objeto')
    concepto = request.GET.get('concepto')
    
    # Filtra los datos según los parámetros
    queryset = BonificacionPermanente.objects.filter(anio=anio, mes=mes, objeto=objeto, concepto=concepto)

    # Crea un libro de trabajo y una hoja
    wb = Workbook()
    ws = wb.active
    ws.title = "Bonificaciones"

    # Define los encabezados
    headers = ["Anio", "Mes", "Objeto", "Concepto", "Subconcepto", "Actividad", "Dpto", "Cedula", "Nombre", "Categoria", "Presupuesto", "Calculo", "Porcentaje", "Devengado", "Jubilacion", "Liquido", "CCargo", "TipoCargo", "CInsti"]
    ws.append(headers)

    # Agrega los datos
    for bonificacion in queryset:
        ws.append([
            bonificacion.anio,
            bonificacion.mes,
            str(bonificacion.objeto),  # Convertir a string
            str(bonificacion.concepto),  # Convertir a string
            bonificacion.subconcepto,
            bonificacion.actividad,
            bonificacion.dpto,
            bonificacion.cedula,
            bonificacion.nombre,
            bonificacion.categoria,
            bonificacion.presupuesto,
            bonificacion.calculo,
            bonificacion.porcentaje,
            bonificacion.devengado,
            bonificacion.jubilacion,
            bonificacion.liquido,
            str(bonificacion.ccargo),  # Convertir a string
            str(bonificacion.tipo_cargo),  # Convertir a string
            str(bonificacion.cinsti)  # Convertir a string
        ])

    # Guarda el archivo en memoria
    output = BytesIO()
    wb.save(output)
    output.seek(0)

    # Prepara la respuesta HTTP
    response = HttpResponse(output,
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="bonificaciones.xlsx"'
    return response
