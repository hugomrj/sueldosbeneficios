# reportes/bonificacion_permanente_reports.py
from django.db import connection
import openpyxl
from django.http import HttpResponse




def generar_bonificacion_permanente_excel(anio, mes, objeto, concepto):
    # Crear un libro de Excel
    wb = openpyxl.Workbook()
    ws = wb.active
    # ws.title = 'Bonificación Permanente'

    # Agregar una fila inicial para la imputación
    # encabezados = ['Imputación', '', '', '', '', '', '', '']
    #  ws.append(encabezados)
    
    # Escribir el resto de los encabezados (sin imputación)
    # ws.append(['Cédula', 'Nombre', 'Cargo', 'Categoría', 'Devengado', 'Jubilación', 'Líquido'])

    # Definir la consulta SQL cruda
    query = """
        SELECT imputacion, cedula, nombre, mcrg.DSC_CARGO, categoria, devengado, jubilacion, liquido
        FROM bonificaciones_permanentes, mcrg
        WHERE anio = %s
        AND mes = %s
        AND objeto = %s
        AND concepto = %s
        AND bonificaciones_permanentes.ccargo = mcrg.ccargo
        ORDER BY imputacion, cedula
    """
    
    # Parámetros de la consulta
    params = [anio, mes, objeto, concepto]

    # Ejecutar la consulta SQL cruda
    with connection.cursor() as cursor:
        cursor.execute(query, params)
        resultados = cursor.fetchall()

    # Variable para controlar la primera aparición de cada imputación
    imputacion_actual = None
    first_record = True

    # Escribir los resultados en las filas del Excel
    for fila in resultados:
        imputacion, cedula, nombre, cargo, categoria, devengado, jubilacion, liquido = fila

        # Verificar si la imputación cambió
        if imputacion != imputacion_actual:
            # Agregar la imputación en una fila separada
            
            if not first_record:
                ws.append([''])             
            
            
            ws.append(['Cédula', 'Nombre', 'Cargo', 'Categoría', 'Devengado', 'Jubilación', 'Líquido'])
            
            ws.append(['Imputación', imputacion, '', '', '', '', ''])
            imputacion_actual = imputacion
            first_record = False

        # Escribir los datos sin la imputación en la fila
        ws.append([cedula, nombre, cargo, categoria, devengado, jubilacion, liquido])

    # Preparar la respuesta HTTP para devolver el archivo Excel
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = f'attachment; filename="bonificacion_permanente_{anio}_{mes}.xlsx"'

    # Guardar el archivo Excel en la respuesta
    wb.save(response)
    
    return response

