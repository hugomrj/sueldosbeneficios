from django.db import models

from django.db import models

from app.models.mins_model import Mins



class LiquidacionRERA (models.Model):    
    
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField(blank=True, null=True)
    mes = models.IntegerField(blank=True, null=True)
    objeto = models.IntegerField(blank=True, null=True)
    cedula = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    
    
    id_depende = models.ForeignKey(Mins, on_delete=models.CASCADE, db_column='id_depende')    
    id_dep_sup = models.ForeignKey(Mins, on_delete=models.CASCADE, 
                db_column='id_dep_sup', related_name='id_dep_sup')
    
    ccargo = models.IntegerField(blank=True, null=True)
    dsc_cargo = models.CharField(max_length=15, blank=True, null=True)

    tope_hs_re = models.IntegerField(blank=True, null=True)
    tope_hs_ra = models.IntegerField(blank=True, null=True)
    
    categoria = models.CharField(max_length=5, blank=True, null=True)
    presupuesto = models.IntegerField(blank=True, null=True)

    horas_re = models.DecimalField(max_digits=3,  decimal_places=2)
    categorias = models.CharField(max_length=20, blank=True, null=True)
    may_asigna = models.IntegerField(blank=True, null=True)
    tot_asigna = models.IntegerField(blank=True, null=True)
    ba_calculo = models.IntegerField(blank=True, null=True)
    calculo = models.IntegerField(blank=True, null=True)
    porcentaje = models.IntegerField(blank=True, null=True)
    
    hs_aplicad = models.DecimalField(max_digits=5,  decimal_places=2)
    cantidad_rubros = models.IntegerField(blank=True, null=True)
    montohs50 = models.IntegerField(blank=True, null=True)
    
    
    monto = models.IntegerField(blank=True, null=True)
    jubilacion = models.IntegerField(blank=True, null=True)
    liquido = models.IntegerField(blank=True, null=True)
    
    
    condicion_laboral = models.IntegerField(blank=True, null=True)
    nro_plani = models.IntegerField(blank=True, null=True)    
    nro_imputa  = models.IntegerField(blank=True, null=True)    
    

    usuario_agrega = models.IntegerField(null=True, blank=True)    
    fecha_agrega = models.DateTimeField(auto_now_add=True, null=True, blank=True)    
    usuario_edita = models.IntegerField(null=True, blank=True)    
    fecha_edita = models.DateTimeField(null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'liquidacion_re'


