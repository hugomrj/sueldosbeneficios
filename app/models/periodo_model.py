from django.db import models

from .concepto_nomina_model import ConceptoNomina
from .objeto_gasto_model import ObjetoGasto


from simple_history.models import HistoricalRecords


class Periodo(models.Model):
    
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    anio = models.IntegerField(db_column='ANIO', blank=True, null=True)  # Field name made lowercase.
    mes = models.IntegerField(db_column='MES', blank=True, null=True)  # Field name made lowercase.
            
    #tipo_plani =  models.ForeignKey(TipoPlanilla, on_delete=models.CASCADE, db_column='tipo_plani')        
    tipo_funci = models.IntegerField(db_column='TIPO_FUNCI', blank=True, null=True)  
    
    numero = models.IntegerField(db_column='NUMERO')  # Field name made lowercase.
    obs = models.TextField(db_column='OBS', blank=True, null=True) 
    
    cerrado = models.IntegerField(db_column='CERRADO')  # Field name made lowercase.
    
    objeto = models.ForeignKey(ObjetoGasto, on_delete=models.CASCADE, db_column='objeto', to_field='codigo_objeto_gasto')
    
    concepto = models.ForeignKey(ConceptoNomina, on_delete=models.CASCADE, db_column='concepto', to_field='codigo_concepto_nomina')
    
    

    historial = HistoricalRecords(table_name='periodo_liquidacion_log')

    class Meta:
        managed = False
        db_table = 'periodo_liquidacion'
        #unique_together = (('anio', 'mes', 'tipo_plani', 'tipo_funci', 'numero'),)


