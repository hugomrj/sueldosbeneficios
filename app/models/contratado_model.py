from django.db import models

from app.models.mins_model import Mins





class Contratados(models.Model):
    mes = models.IntegerField(blank=True, null=True)
    anio = models.IntegerField(blank=True, null=True)
    objeto = models.IntegerField(blank=True, null=True)
    cedula = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
        
    id_depende = models.ForeignKey(Mins, on_delete=models.CASCADE, db_column='id_depende')    
        
    dependencia = models.CharField(max_length=100, blank=True, null=True)
    nro_contrato = models.CharField(max_length=45, blank=True, null=True)
    presupuesto = models.IntegerField(blank=True, null=True)
    devengado = models.IntegerField(blank=True, null=True)
    iva_30 = models.IntegerField(blank=True, null=True)
    iva_10 = models.IntegerField(blank=True, null=True)
    multa = models.IntegerField(blank=True, null=True)
    judiciales = models.IntegerField(blank=True, null=True)
    liquido = models.IntegerField(blank=True, null=True)
    contribuyente = models.IntegerField(blank=True, null=True)
    tiene_judicial = models.IntegerField(blank=True, null=True)
    id_estado_pago = models.IntegerField(blank=True, null=True)
    gestion = models.CharField(max_length=45, blank=True, null=True)
    nro_planilla = models.IntegerField(blank=True, null=True)
    
    usuario_agrega = models.IntegerField(blank=True, null=True)
    fecha_agrega = models.DateTimeField(blank=True, null=True)
    usuario_edita = models.IntegerField(blank=True, null=True)
    fecha_edita = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contratados'

