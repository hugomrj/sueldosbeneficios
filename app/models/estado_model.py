from django.db import models


class Estado(models.Model):
    estado = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'estados'

    def __str__(self):
        return self.descripcion
    
    
    