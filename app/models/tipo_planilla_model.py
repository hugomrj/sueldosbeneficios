from django.db import models


# Create your models here.
class TipoPlanilla(models.Model):
    
    id = models.PositiveIntegerField(primary_key=True)
    des_plani = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_planilla'

    def __str__(self):
        return self.des_plani
    






