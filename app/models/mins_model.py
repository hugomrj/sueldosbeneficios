
from django.db import models

class Mins(models.Model):
    cinsti = models.CharField(max_length=255, primary_key=True)
    nombre = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'mins'
    
    def __str__(self):
        return self.nombre