from django.db import models


class ConceptoNomina(models.Model):        
    
    #id_concepto_nomina = models.AutoField()
    codigo_concepto_nomina = models.PositiveIntegerField(primary_key=True)
    
    concepto = models.CharField(max_length=100)
    migrar = models.IntegerField(blank=True, null=True)
    sueldo_contratado = models.IntegerField(blank=True, null=True)
    og = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'concepto_nomina'
        #unique_together = (('id_concepto_nomina', 'codigo_concepto_nomina'),)


    def __str__(self):
        codigo = self.codigo_concepto_nomina if self.codigo_concepto_nomina is not None else ''
        nombre = self.concepto if self.concepto is not None else ''
        return f'{codigo} - {nombre}'


