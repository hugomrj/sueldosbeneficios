from django.db import models

from app.models.mins_model import Mins


from simple_history.models import HistoricalRecords



class RemuneracionPedido (models.Model):
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField(blank=True, null=True)
    mes = models.IntegerField(blank=True, null=True)
    cedula = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    
    objeto = models.IntegerField(blank=True, null=True)            
       
    id_depende = models.ForeignKey(Mins, on_delete=models.CASCADE, db_column='id_depende')
    
    horas = models.IntegerField(blank=True, null=True)
    horas_ra = models.IntegerField(blank=True, null=True)
    expediente = models.IntegerField(blank=True, null=True)
    memorandum = models.IntegerField(blank=True, null=True)
    actualizo = models.IntegerField(blank=True, null=True)
    observacion = models.CharField(max_length=150, blank=True, null=True)
    
    cod_og = models.IntegerField(blank=True, null=True)
    encontro = models.CharField(max_length=45, blank=True, null=True)
        
    usuario = models.IntegerField(null=True, blank=True)    
    
    historial = HistoricalRecords(table_name='remuneraciones_pedidos_log')
    

    class Meta:
        managed = False
        db_table = 'remuneraciones_pedidos'

