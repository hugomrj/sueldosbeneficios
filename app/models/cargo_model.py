from django.db import models




class Cargo(models.Model):
    ccargo = models.PositiveIntegerField(db_column='CCARGO', primary_key=True)  # Field name made lowercase.
    dsc_cargo = models.CharField(db_column='DSC_CARGO', max_length=100)  # Field name made lowercase.
    tipo_cargo = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mcrg'
        
    def __str__(self):
        return self.dsc_cargo        
        

