from django.db import models

from app.models.cargo_model import Cargo
from app.models.estado_model import Estado
from app.models.mins_model import Mins



from .objeto_gasto_model import ObjetoGasto
from .concepto_nomina_model import ConceptoNomina

from simple_history.models import HistoricalRecords


class BonificacionPedido(models.Model):

    id = models.AutoField(primary_key=True)
    anio = models.IntegerField(blank=True, null=True)
    objeto = models.ForeignKey(
        ObjetoGasto,
        on_delete=models.CASCADE,
        db_column="objeto",
        to_field="codigo_objeto_gasto",
    )
    concepto = models.ForeignKey(
        ConceptoNomina,
        on_delete=models.CASCADE,
        db_column="concepto",
        to_field="codigo_concepto_nomina",
    )

    subconcepto = models.IntegerField(blank=True, null=True)
    cargo113 = models.IntegerField(blank=True, null=True)

    cedula = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)
    cinsti = models.ForeignKey(Mins, on_delete=models.CASCADE, db_column="cinsti")
    ccargo = models.ForeignKey(
        Cargo, on_delete=models.CASCADE, db_column="ccargo", to_field="ccargo"
    )
    decreto = models.CharField(max_length=200, blank=True, null=True)
    resolucion_pago = models.CharField(max_length=200, blank=True, null=True)
    porcentaje = models.IntegerField(blank=True, null=True)

    estado = models.ForeignKey(Estado, on_delete=models.CASCADE, db_column="estado")
    imputacion =  models.CharField(max_length=100, blank=True, null=True)

    tipo_cargo = models.IntegerField(blank=True, null=True)
    presupuestado = models.IntegerField(blank=True, null=True)

    expediente = models.CharField(max_length=45, blank=True, null=True)
    fecha_pedido = models.DateField(blank=True, null=True)
    obs = models.TextField(db_column='OBS', blank=True, null=True) 

    historial = HistoricalRecords(table_name="bonificaciones_pedidos_log")

    class Meta:
        managed = False
        db_table = "bonificaciones_pedidos"



