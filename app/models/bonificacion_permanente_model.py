from django.db import models

from app.models.beneficio_estado import BeneficioEstado
from app.models.cargo_model import Cargo
from app.models.concepto_nomina_model import ConceptoNomina
from app.models.mins_model import Mins
from app.models.objeto_gasto_model import ObjetoGasto
from app.models.tipo_cargo_model import TipoCargo

from simple_history.models import HistoricalRecords



class BonificacionPermanente(models.Model):

    id = models.AutoField(primary_key=True)
    anio = models.IntegerField()
    mes = models.IntegerField()
    objeto = models.ForeignKey(
        ObjetoGasto,
        on_delete=models.CASCADE,
        db_column="objeto",
        to_field="codigo_objeto_gasto",
    )
    concepto = models.ForeignKey(
        ConceptoNomina,
        on_delete=models.CASCADE,
        db_column="concepto",
        to_field="codigo_concepto_nomina",
    )
    subconcepto = models.IntegerField(blank=True, null=True)
    actividad = models.IntegerField(blank=True, null=True)
    dpto = models.IntegerField(blank=True, null=True)
    cedula = models.IntegerField()
    nombre = models.CharField(max_length=250, blank=True, null=True)
    categoria = models.CharField(max_length=255, blank=True, null=True)

    presupuesto = models.IntegerField(blank=True, null=True)
    calculo = models.IntegerField(blank=True, null=True)
    porcentaje = models.IntegerField(blank=True, null=True)
    devengado = models.IntegerField(blank=True, null=True)
    jubilacion = models.IntegerField()
    liquido = models.PositiveIntegerField()

    ccargo = models.ForeignKey(
        Cargo, on_delete=models.CASCADE, db_column="ccargo", to_field="ccargo"
    )
    tipo_cargo = models.ForeignKey(
        TipoCargo,
        on_delete=models.CASCADE,
        db_column="tipo_cargo",
        to_field="tipo_cargo",
    )
    cinsti = models.ForeignKey(Mins, on_delete=models.CASCADE, db_column="cinsti")

    planilla = models.IntegerField(blank=True, null=True)
    forma_pago = models.PositiveIntegerField(blank=True, null=True)
    
    estado = models.ForeignKey(
        BeneficioEstado,         on_delete=models.PROTECT, 
        null=True,         db_column='estado',          to_field='estado'  
    )
    
    observacion = models.CharField(max_length=250, blank=True, null=True)
    id_pedido = models.IntegerField(blank=True, null=True)
    imputacion =  models.CharField(max_length=100, blank=True, null=True)

    monto113 = models.IntegerField(blank=True, null=True)
    expediente = models.CharField(max_length=45, blank=True, null=True)

    historial = HistoricalRecords(table_name="bonificaciones_permanentes_log")

    class Meta:
        managed = False
        db_table = "bonificaciones_permanentes"
        unique_together = (
            ("anio", "mes", "objeto", "concepto", "cedula", "subconcepto"),
        )

