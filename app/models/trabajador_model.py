from django.db import models
    
class Trabajador(models.Model):
    cedula = models.PositiveIntegerField()
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    nombre = models.CharField(max_length=100)
    letra_sigmec = models.CharField(max_length=1)
    letra_sinarh = models.CharField(max_length=1)
    correo = models.CharField(max_length=50, blank=True, null=True)
    celular = models.CharField(max_length=25, blank=True, null=True)
    contratado = models.IntegerField()
    id_trabajador = models.AutoField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'trabajador'
    
    
    
    
