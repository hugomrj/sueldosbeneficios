from django.db import models


class ObjetoGasto(models.Model):

    codigo_objeto_gasto = models.IntegerField(db_column='CODIGO_OBJETO_GASTO', primary_key=True)  # Field name made lowercase.
    nombre_objeto_gasto = models.CharField(db_column='NOMBRE_OBJETO_GASTO', max_length=100, blank=True, null=True)  # Field name made lowercase.
    imputacion = models.CharField(db_column='IMPUTACION', max_length=30, blank=True, null=True)  # Field name made lowercase.
    id_subgrupo_gasto = models.IntegerField(db_column='ID_SUBGRUPO_GASTO', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'objeto_gastos'

    def __str__(self):
        codigo = self.codigo_objeto_gasto if self.codigo_objeto_gasto is not None else ''
        nombre = self.nombre_objeto_gasto if self.nombre_objeto_gasto is not None else ''
        return f'{codigo} - {nombre}'
    



