from django.db import models

from app.models.mins_model import Mins




class MarcacionDetalle(models.Model):
    
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField(blank=True, null=True)
    mes = models.IntegerField(blank=True, null=True)
    
    codigo_trabajador = models.IntegerField(blank=True, null=True)
    nombre_trabajador = models.CharField(max_length=100, blank=True, null=True)
    
    id_depende = models.ForeignKey(
            Mins,    on_delete=models.CASCADE,
            db_column='codigo_dependencia',  to_field='cinsti'  )
        
    cantidad_horas_remuneracion_extraordinaria = models.IntegerField(blank=True, null=True)
    cantidad_horas_remuneracion_adicional = models.IntegerField(blank=True, null=True)
    codigo_categoria_rubro = models.CharField(max_length=3, blank=True, null=True)
    monto_categoria_rubro = models.IntegerField(blank=True, null=True)    
    cantidad_rubro = models.IntegerField(blank=True, null=True)
    total_asignacion = models.IntegerField(blank=True, null=True)
         
        
        
    usuario_agrega = models.IntegerField(null=True, blank=True)        
    fecha_agrega = models.DateTimeField(auto_now_add=True)    
    usuario_edita = models.IntegerField(null=True, blank=True)    
    fecha_edita = models.DateTimeField(null=True, blank=True)
    



    class Meta:
        managed = False
        db_table = 'marcacion_detalle'



