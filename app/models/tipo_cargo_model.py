from django.db import models



class TipoCargo(models.Model):
    tipo_cargo = models.IntegerField(db_column='TIPO_CARGO', blank=True, primary_key=True)  # Field name made lowercase.
    nombre_cargo = models.CharField(db_column='NOMBRE_CARGO', max_length=30, blank=True, null=True)  # Field name made lowercase.
    porcentaje_certificacion_documental_evaluacion = models.FloatField(db_column='PORCENTAJE_CERTIFICACION_DOCUMENTAL_EVALUACION', blank=True, null=True)  # Field name made lowercase.
    porcentaje_prueba_escrita_evaluacion = models.FloatField(db_column='PORCENTAJE_PRUEBA_ESCRITA_EVALUACION', blank=True, null=True)  # Field name made lowercase.
    porcentaje_prueba_oral_evaluacion = models.FloatField(db_column='PORCENTAJE_PRUEBA_ORAL_EVALUACION', blank=True, null=True)  # Field name made lowercase.
    clave = models.CharField(max_length=2, blank=True, null=True)
    presupuestado = models.IntegerField(blank=True, null=True)
    categoria = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_cargo'

    def __str__(self):
        return self.nombre_cargo      
        
    

