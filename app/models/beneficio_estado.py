
from django.db import models

class BeneficioEstado(models.Model):
    estado = models.CharField(max_length=255, primary_key=True)
    descripcion = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'beneficios_estados'
    
    def __str__(self):
        return self.descripcion
    
    