from django.db import models

from simple_history.models import HistoricalRecords



class BonificacionesPermanentesPagadas(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    id_periodo = models.IntegerField(
        db_column="ID_PERIODO"
    )  # Field name made lowercase.
    
    id_bonificacion = models.IntegerField(db_column="id_bonificacion")  # Field name made lowercase.

    devengado = models.IntegerField(db_column="DEVENGADO")  # Field name made lowercase.    
    id_estado = models.IntegerField(db_column="ID_ESTADO")  # Field name made lowercase.
    

    historial = HistoricalRecords(table_name="bonificaciones_permanentes_pagadas_log")

    class Meta:
        managed = False
        db_table = "bonificaciones_permanentes_pagadas"
