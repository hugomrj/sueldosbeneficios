from django.contrib import admin
from django.contrib.auth.models import Permission

# Register your models here.
class PermissionAdmin(admin.ModelAdmin):
    # Método para concatenar content_type y codename en el formato deseado
    def concatenar_content_type_codename(self, obj):
        return f"{obj.content_type.app_label}.{obj.codename}"
    
    # Nombre de la columna personalizada en el admin
    concatenar_content_type_codename.short_description = 'Content Type - Codename'

    # Agregar la columna concatenada en list_display
    list_display = ('name', 'concatenar_content_type_codename')  # Ordena las columnas como desees
    search_fields = ('name', 'codename')

admin.site.register(Permission, PermissionAdmin)