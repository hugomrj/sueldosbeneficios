from django.db.models import Q
from django.http import JsonResponse

from app.models.mins_model import Mins



def mins_results(request):
    
    # Obtén el valor de búsqueda del parámetro 'q' en la URL
    valor_busqueda = request.GET.get('q', '')
        
    # Dividir el término de búsqueda en palabras y crear una consulta combinada
    consulta = Q()
    for buscar in valor_busqueda.split():
        consulta &= Q(nombre__icontains=buscar)          
    

    # Filtra los clientes basados en el valor de búsqueda
    filtrados = Mins.objects.filter(consulta)

    # Crea una lista de diccionarios con los resultados
    resultados = [{'id': reg.cinsti, 'text': reg.nombre} for reg in filtrados]

    return JsonResponse(resultados, safe=False)