




from django.db import connections
from django.http import Http404, HttpResponse, JsonResponse

from app.models.periodo_model import Periodo


def generar_numero_planilla(request):
    
    # Obtener parámetros del request (anio, mes, objeto, concepto)
    anio = request.GET.get('anio')
    mes = request.GET.get('mes')
    objeto = request.GET.get('objeto')
    concepto = request.GET.get('concepto')
    tipo_funci = request.GET.get('tipo_funci')    
    
    # Ejecutar la consulta SQL para obtener el número máximo
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT max(numero)
            FROM sueldosbeneficios.periodo_liquidacion
            WHERE anio = %s
              AND mes = %s
              AND objeto = %s
              AND concepto = %s
              and tipo_funci = %s
        """, [anio, mes, objeto, concepto, tipo_funci])
        numero_maximo = cursor.fetchone()[0]  

    # Verificar si numero_maximo es None (cuando no hay registros)
    if numero_maximo is None:
        numero_maximo = 0  # Establecer un valor predeterminado o manejar según tu lógica de negocio

    # Sumar 1 al número máximo obtenido
    numero_maximo += 1

    # Retornar el valor sumado como texto plano en la respuesta HTTP
    return HttpResponse(str(numero_maximo))








def periodo_filtrar(request):

    # Obtenemos los parámetros desde la solicitud GET (o POST, según prefieras)
    anio = request.GET.get('anio', None)
    mes = request.GET.get('mes', None)
    objeto = request.GET.get('objeto', None)
    concepto = request.GET.get('concepto', None)

    # Filtramos el queryset basándonos en los parámetros proporcionados
    queryset = Periodo.objects.all()

    if anio:
        queryset = queryset.filter(anio=anio)
    
    if mes:
        queryset = queryset.filter(mes=mes)

    if objeto:
        queryset = queryset.filter(objeto=objeto)
    
    if concepto:
        queryset = queryset.filter(concepto=concepto)
    
    
    # Crea una lista de diccionarios con los resultados
    resultados = [
        {
            'id': p.id,
            'obs': p.obs
        }
        for p in queryset
    ]

    # Retorna la lista como respuesta en formato JSON
    return JsonResponse(resultados, safe=False)




def get_periodo_by_id(request):
    
    # Obtenemos el parámetro 'id' desde la solicitud GET
    periodo_id = request.GET.get('id', None)

    # Intentamos obtener el registro filtrado por 'id'
    try:
        periodo = Periodo.objects.get(id=periodo_id)
        data = {
            'id': periodo.id,
            'obs': periodo.obs,  # Ajusta esto según los campos que quieras retornar
            'anio': periodo.anio,
            'mes': periodo.mes,
            'objeto': str(periodo.objeto), 
            'concepto': str(periodo.concepto),
            'cerrado': periodo.cerrado
        }
    except Periodo.DoesNotExist:
         data = {}

    # Retornamos una respuesta JSON con los datos del registro encontrado
    return JsonResponse(data)