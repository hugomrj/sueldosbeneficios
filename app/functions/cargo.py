from django.db.models import Q
from django.http import JsonResponse
from app.models.cargo_model import Cargo


def cargo_filtrar(request):
    # Obtén el valor de búsqueda del parámetro 'q' en la URL
    valor_busqueda = request.GET.get('q', '')
    
    
    # Dividir el término de búsqueda en palabras y crear una consulta combinada
    consulta = Q()
    for buscar in valor_busqueda.split():
        consulta &= Q(dsc_cargo__icontains=buscar)  
    
    
    # Filtra los cargos basados en el valor de búsqueda
    filtrados = Cargo.objects.filter(consulta)
    

    # Crea una lista de diccionarios con los resultados
    resultados = [{'id': reg.ccargo, 'text': reg.dsc_cargo} for reg in filtrados]    

    return JsonResponse(resultados, safe=False)




