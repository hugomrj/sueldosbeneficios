  
from django.http import JsonResponse
from app.models.concepto_nomina_model import ConceptoNomina




def filtrar_conceptos_objeto(request, obj):
    if obj:
        conceptos = ConceptoNomina.objects.filter(og=obj).values('codigo_concepto_nomina', 'concepto', 'id_concepto_nomina')  
        return JsonResponse(list(conceptos), safe=False)
    else:
        return JsonResponse([], safe=False)    
    
    






def concepto_nomina_filtrar(request):
    # Obtén el valor de búsqueda del parámetro 'og' en la URL
    og_value = request.GET.get('og', None)
    
    # Filtra los conceptos de nómina basados en el valor de 'og'
    if og_value is not None:
        conceptos = ConceptoNomina.objects.filter(og=og_value)
    else:
        conceptos = ConceptoNomina.objects.all()

    
    # Crea una lista de diccionarios con los resultados
    resultados = [
        {
            'codigo': c.codigo_concepto_nomina,
            'concepto': f'{c.codigo_concepto_nomina} - {c.concepto}'
        }
        for c in conceptos
    ]
    

    return JsonResponse(resultados, safe=False)    