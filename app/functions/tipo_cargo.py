from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from app.models.tipo_cargo_model import TipoCargo



def tipo_cargo_filtrar(request, id):
    try:
        tipo_cargo_id = int(id)
    except ValueError:
        return JsonResponse({'error': 'ID de tipo cargo inválido'}, status=400)
    
    try:
        tipo_cargo = get_object_or_404(TipoCargo, tipo_cargo=tipo_cargo_id)
        data = {
            'tipo_cargo': tipo_cargo.tipo_cargo,
            'nombre_cargo': tipo_cargo.nombre_cargo,
            'porcentaje_certificacion_documental': tipo_cargo.porcentaje_certificacion_documental_evaluacion,
            'porcentaje_prueba_escrita': tipo_cargo.porcentaje_prueba_escrita_evaluacion,
            'porcentaje_prueba_oral': tipo_cargo.porcentaje_prueba_oral_evaluacion,
            'clave': tipo_cargo.clave,
            'presupuestado': tipo_cargo.presupuestado,
        }
        return JsonResponse(data)
    except Http404:
        return JsonResponse({'error': 'Tipo de cargo no encontrado'}, status=404)
