from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404

from app.models.trabajador_model import Trabajador


def trabajador_nombre_apellido(request, cedula):
    try:
        cedula = int(cedula)
    except ValueError:
        return JsonResponse({'error': 'Cédula inválida'}, status=400)
    
    try:
        trabajador = get_object_or_404(Trabajador, cedula=cedula)
        nombre_completo = f'{trabajador.nombres} {trabajador.apellidos}'
        return JsonResponse({'nombre': nombre_completo})
    except Http404:
        return JsonResponse({'error': 'Trabajador no encontrado'}, status=404)
    
    

