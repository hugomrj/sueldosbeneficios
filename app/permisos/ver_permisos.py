import os
import django
import sys

# Agregar la raíz del proyecto al PYTHONPATH
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
# Ver el contenido de sys.path
#print("Contenido de sys.path:")
print(sys.path)


# Configurar Django si es necesario
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from django.contrib.auth.models import User

def verificar_permisos(username):
    """Verifica y muestra los permisos de un usuario específico."""
    try:
        usuario = User.objects.get(username=username)

        print(f"Permisos del usuario '{username}':\n")

        # Obtener todos los permisos asignados directamente o a través de grupos
        permisos = usuario.get_all_permissions()

        if permisos:
            for permiso in permisos:
                print(f"✔ {permiso}")
        else:
            print("No tiene permisos asignados.")            



    except User.DoesNotExist:
        print(f"El usuario '{username}' no existe.")

if __name__ == "__main__":
    # Verificar si se pasó un argumento
    if len(sys.argv) < 2:
        print("Debes proporcionar un nombre de usuario.")
        print("Uso: python ver_permisos.py hugo")
        sys.exit(1)

    # Obtener el nombre de usuario desde los argumentos
    username = sys.argv[1]
    verificar_permisos(username)

    
