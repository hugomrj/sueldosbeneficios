from django.http import JsonResponse
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views import View



class SocioCrearView(PermissionRequiredMixin, LoginRequiredMixin, View):
    permission_required = 'auth.ver_informes'
        
    def has_permission(self):    
        return (self.request.user.has_perm(self.permission_required))
    
    def get(self, request, *args, **kwargs):
        if self.has_permission():
            return JsonResponse({'tiene_permiso': True, 'usuario': request.user.username})
        return JsonResponse({'tiene_permiso': False, 'usuario': request.user.username})