from django.urls import path

from app.views.periodo_views import PeriodoList
from app.views.periodo_views import PeriodoCreate
from app.views.periodo_views import PeriodoDetail
from app.views.periodo_views import PeriodoEdit
from app.views.periodo_views import PeriodoDelete

from app.functions.periodo import generar_numero_planilla

app_name = 'periodo'

urlpatterns = [
    
   path('list', PeriodoList.as_view(), name='list'),
   path('list/<int:anio>/', PeriodoList.as_view(), name='list'),    
   
   path('create/<int:anio>/', PeriodoCreate.as_view(), name='create'),   
   path('detail/<int:pk>/', PeriodoDetail.as_view(), name='detail'),           
   path('edit/<int:pk>/', PeriodoEdit.as_view(), name='edit'),  
   path('delete/<int:pk>/', PeriodoDelete.as_view(), name='delete'),        
   
   path('generar_numero_planilla/', generar_numero_planilla, name='generar_numero_planilla'),
    
]    

     



