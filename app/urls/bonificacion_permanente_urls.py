
from django.urls import path


from app.views.bonificacion_permanente_views import (
    BonificacionPermanenteEdit,  BonificacionPermanenteList,  
    BonificacionPermanenteDetail, BonificacionPermanenteTablePartialView,
    exportar_excel_bonificacion
)



app_name = 'bonificacion_permanente'

urlpatterns = [    
      
    path('list/', BonificacionPermanenteList.as_view(), name='list'),
    path('list/<int:anio>/<int:mes>/<int:obj>/<str:concepto>/', BonificacionPermanenteList.as_view(), name='list'),       
    
    path('edit/<int:pk>/', BonificacionPermanenteEdit.as_view(),name='edit'),          
        
    path('detail/<int:pk>/', BonificacionPermanenteDetail.as_view(),name='detail'),          
    
    # tamplate parcial
    path('table_partial/', BonificacionPermanenteTablePartialView.as_view(), name='table_partial'),

     
    # reporte         
    path('exportar_excel/', exportar_excel_bonificacion, name='exportar_excel'),

        
   

]    





















