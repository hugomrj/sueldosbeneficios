from django.urls import path

from app.views.remuneracion_pedido_views import RemuneracionesPedidosLista
from app.views.remuneracion_pedido_views import RemuneracionPedidoDetalles
from app.views.remuneracion_pedido_views import RemuneracionPedidoCrear
from app.views.remuneracion_pedido_views import RemuneracionPedidoEditar
from app.views.remuneracion_pedido_views import RemuneracionPedidoBorrar
from app.views.remuneracion_pedido_views import RemuneracionPedidoReplicarLista



app_name = 'remuneraciones_pedidos'

urlpatterns = [
    
    path('lista', RemuneracionesPedidosLista.as_view(), name='lista'),
    path('lista/<int:anio>/<int:mes>/', RemuneracionesPedidosLista.as_view(), name='lista'),    
    
    path('detalles/<int:pk>/', RemuneracionPedidoDetalles.as_view(), name='detalle'),        
    path('crear/<int:anio>/<int:mes>/', RemuneracionPedidoCrear.as_view(), name='crear'),
    path('editar/<int:pk>/', RemuneracionPedidoEditar.as_view(), name='editar'),        
    
    path('borrar/<int:pk>/', RemuneracionPedidoBorrar.as_view(), name='borrar'),        
    
    path('replicarlista', RemuneracionPedidoReplicarLista.as_view(), name='replicarlista'),        
    
    
]    
    


