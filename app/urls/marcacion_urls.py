from django.urls import path

from app.views.marcacion_detalle_views import MarcacionDetalleDelete
from app.views.marcacion_detalle_views import MarcacionDetalleDetail
from app.views.marcacion_detalle_views import MarcacionDetalleCreate
from app.views.marcacion_detalle_views import MarcacionDetalleEdit
from app.views.marcacion_detalle_views import MarcacionDetalleList

from app.views.marcacion_detalle_views import MarcacionMigrarArchivo



app_name = 'marcacion_detalle'


urlpatterns = [      
    path('list', MarcacionDetalleList.as_view(), name='list'),
    path('list/<int:anio>/<int:mes>/', MarcacionDetalleList.as_view(), name='list'),    

    path('detail/<int:pk>/', MarcacionDetalleDetail.as_view(), name='detail'), 
    path('create/<int:anio>/<int:mes>/', MarcacionDetalleCreate.as_view(), name='create'),
    path('edit/<int:pk>/', MarcacionDetalleEdit.as_view(), name='edit'),  
    path('delete/<int:pk>/', MarcacionDetalleDelete.as_view(), name='delete'),  

    path('migrar/', MarcacionMigrarArchivo.as_view(), name='migrar'),

]