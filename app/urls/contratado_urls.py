
from django.urls import path
from accounts.views import Login, Home


from app.views.contrato_views import ContratadoMigrar



app_name = 'contratado'


urlpatterns = [  
    # contratados
    path('migrar/', ContratadoMigrar.as_view(), name='migrar'),
]
    


