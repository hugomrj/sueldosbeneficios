
from django.urls import path
from app.views import liquidacion_rera_views

app_name = 'liquidacion_rera'

urlpatterns = [
    
   path('lista', liquidacion_rera_views.LiquidacionRERALista.as_view(), name='lista'),
   path('lista/<int:anio>/<int:mes>/', liquidacion_rera_views.LiquidacionRERALista.as_view(), name='lista'),    
   
   path('detalles/<int:pk>/', liquidacion_rera_views.LiquidacionRERADetalles.as_view(), name='detalle'), 
   
   path('crear/<int:anio>/<int:mes>/', liquidacion_rera_views.LiquidacionRERACrear.as_view(), name='crear'),
   path('editar/<int:pk>/', liquidacion_rera_views.LiquidacionRERAEditar.as_view(), name='editar'),  
   path('borrar/<int:pk>/', liquidacion_rera_views.LiquidacionRERABorrar.as_view(), name='borrar'),  

   path('procesar/', liquidacion_rera_views.LiquidacionRERA_procesar.as_view(), name='procesar'),  
   
    


]    

     


