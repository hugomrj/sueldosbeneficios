
from django.urls import path

from app.views.bonificacion_pedido_views import (
    BonificacionPedidoList, BonificacionPedidoCreate, 
    BonificacionPedidoDetail, BonificacionPedidoEdit, 
    BonificacionPedidoDelete
)


app_name = 'bonificacion_pedido'

urlpatterns = [  
    # Rutas para BonificacionPedido usando el módulo completo
    path('list/', BonificacionPedidoList.as_view(), name='list'),

    path('list/<int:anio>/<int:obj>/<str:concepto>/', BonificacionPedidoList.as_view(), name='list'),      

    path('create/<int:anio>/', BonificacionPedidoCreate.as_view(), name='create'),
    path('detail/<int:pk>/', BonificacionPedidoDetail.as_view(), name='detail'),
    path('edit/<int:pk>/', BonificacionPedidoEdit.as_view(), name='edit'),
    path('delete/<int:pk>/', BonificacionPedidoDelete.as_view(), name='delete'),
]     

