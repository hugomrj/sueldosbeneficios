
/* funciones js que som acccesibles desde todo el proyecto */


// Definir una función llamada comboselect2 que toma un parámetro 'id'
function comboselect2(id) {
    // Obtener el elemento del DOM con el id proporcionado
    var idDependeElement = document.getElementById(id);
  
    // Inicializar el plugin Select2 en el elemento obtenido
    $(idDependeElement).select2({       
      // Opciones de Select2  
      width: '100%',
      
      
      // Configuración AJAX para la búsqueda y obtención de resultados
      ajax: {
        // URL a la que se enviará la solicitud AJAX
        url: '/mins/results/',
  
        // Función que define los datos que se enviarán en la solicitud
        data: function (params) {
          return {
            q: params.term  // 'q' es el término de búsqueda proporcionado por el usuario
          };
        },
        
        // Función que procesa los resultados recibidos de la solicitud AJAX
        processResults: function (data) {
          return {
            results: data  // 'results' debe contener los resultados que Select2 mostrará
          };
        },
      }
    });
  }

  

  



// Definir una función llamada comboselect2 que toma un parámetro 'id'
function comboselect2_cargos(id) {
  // Obtener el elemento del DOM con el id proporcionado
  var idDependeElement = document.getElementById(id);

  // Inicializar el plugin Select2 en el elemento obtenido
  $(idDependeElement).select2({       
    // Opciones de Select2  
    width: '100%',
    
    // Configuración AJAX para la búsqueda y obtención de resultados
    ajax: {
      // URL a la que se enviará la solicitud AJAX
      url: '/cargo/filtrar/',

      // Función que define los datos que se enviarán en la solicitud
      data: function (params) {

        return {
          q: params.term  // 'q' es el término de búsqueda proporcionado por el usuario
        };
      },
      
      // Función que procesa los resultados recibidos de la solicitud AJAX
      processResults: function (data) {
        return {
          results: data  // 'results' debe contener los resultados que Select2 mostrará
        };
      },
    }
  });
}





  

 /**
 * Función para mostrar la pagina despues de terminar de cargarla mientras
 * se muestra un spiner
 */
function mostrarContenido() {
    // Verificar si el elemento 'main_spinner' existe
    var spinnerElement = document.getElementById('main_spinner');
    
    if (spinnerElement) {
        // Ocultar el elemento 'main_spinner' si existe
        spinnerElement.style.display = 'none';
    }

    // Verificar si el elemento 'main_container' existe
    var containerElement = document.getElementById('main_container');
    
    if (containerElement) {
        // Mostrar el elemento 'main_container' si existe
        containerElement.style.display = 'block';
    }
}
// Llamar a la función para ejecutar el código
//mostrarContenido();






/**
 * Deshabilita todos los campos de entrada, áreas de texto y select dentro de un formulario.
 * @param {string} form_id - El ID del formulario que se va a bloquear.
 */
function bloquearCamposFormulario(form_id) {
  var form = document.getElementById(form_id);
  
  if (!form) {
      console.error("El formulario con el ID especificado no existe.");
      return; // Salir de la función si no se encontró el formulario
  }
  
  // Obtener todos los elementos de entrada, áreas de texto y select del formulario
  var inputs = form.querySelectorAll('input');
  var textareas = form.querySelectorAll('textarea');
  var selects = form.querySelectorAll('select');

  // Iterar sobre los elementos de entrada y deshabilitarlos
  for (var i = 0; i < inputs.length; i++) {
      inputs[i].disabled = true;
  }

  // Iterar sobre las áreas de texto y deshabilitarlas
  for (var j = 0; j < textareas.length; j++) {
      textareas[j].disabled = true;
  }

  // Iterar sobre los elementos select y deshabilitarlos
  for (var k = 0; k < selects.length; k++) {
      selects[k].disabled = true;
  }
}
// Ejemplo de uso: bloquearCamposFormulario('formulario');







// Formatear Numeros
function formatearNumero(input_id) {
  // Obtener el elemento del DOM usando el ID
  var input = document.getElementById(input_id);

   // Función para formatear el valor del input se BORRA SI NO FUNCIONA
   function formatValue() {
  
    // Obtener el valor del input
  var valor = input.value;

  // Eliminar los puntos de la cadena y convertirlo a número entero
  var numero = parseInt(valor.replace(/\./g, ''), 10);

  // Formatear el valor con separadores de miles usando puntos
  var numeroFormateado = numero.toLocaleString('es-ES'); // Usar 'es-ES' para forzar el uso de puntos como separadores de miles
   
  input.type = 'text';
  // Actualizar el valor del input con el número formateado
  input.value = numeroFormateado;
   }

   // Llamar a la función de formateo al cargar la página para inicializar el valor
  formatValue();

  // Agregar un event listener para escuchar los cambios en el input y formatear el valor
  input.addEventListener('input', formatValue);
}



function esNumero(input_id) {

  var input = document.getElementById(input_id);
    // Verificar si el valor es numérico
  if (isNaN(input.value) || input.value.trim() === '') {    
    return 0;
  } 
  else{
    return input.value;
  }

}


function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        // Buscar la cookie con el nombre especificado
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
        }
    }
  }
  return cookieValue;
}








function mostrarModalError(mensaje) {

  document.getElementById("modalMensaje").innerText = mensaje;
  let modal = new bootstrap.Modal(document.getElementById("modalError"));
  modal.show();
}