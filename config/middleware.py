# config/middleware.py

from django.utils.functional import SimpleLazyObject
from django.shortcuts import redirect
from django.http import HttpResponseForbidden
from django.http import JsonResponse



def get_role(request):
    if not hasattr(request, '_cached_role'):
        user = getattr(request, 'user', None)
        role_name = None
        if user and user.is_authenticated:
            user_groups = user.groups.all()
            if user_groups.exists():
                role_name = user_groups.first().name
        request._cached_role = role_name            
    return request._cached_role




class RoleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.role = SimpleLazyObject(lambda: get_role(request))
        response = self.get_response(request)

        # Si la respuesta es 403, delegamos a una función para redirigir
        
        if response.status_code == 403:
            return self.handle_forbidden_error(request)
        
        return response



    def handle_forbidden_error(self, request ):
        """
        Maneja el error 403 con dos opciones:
        - Si `json_response` es True, devuelve un JsonResponse con un mensaje de error.
        - Si `json_response` es False, redirige a la vista 'error_permisos'.
        
        :param request: HttpRequest de Django.
        :param json_response: Booleano que indica si se debe devolver JSON o redirigir.
        :return: JsonResponse o HttpResponseRedirect según el caso.
        """
        json_response=True
        if json_response:
            return JsonResponse({'error': 'No tienes permisos para acceder'}, status=403)
        return redirect('error_permisos')