"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.contrib import admin
from django.urls import include, path

from app.functions import cargo, concepto, mins, periodo, trabajador, tipo_cargo

from app.permisos.prueba_views import SocioCrearView

from app.views.error_views import error_permisos 


urlpatterns = [
    
    path('admin/', admin.site.urls),
    
    path('', include('accounts.urls')),  

    path('periodo/', include('app.urls.periodo_urls')),  
    path('remuneraciones_pedidos/', include('app.urls.remuneraciones_pedidos_urls')),  
    path('marcacion_detalle/', include('app.urls.marcacion_urls')),      
    path('liquidacion_rera/', include('app.urls.liquidacion_rera_urls')),  
    path('bonificacion_pedido/', include('app.urls.bonificacion_pedido_urls')),  
    
    path('bonificacion_permanente/', include('app.urls.bonificacion_permanente_urls')),  
    path('bonificacion_permanente_pagada/', include('app.urls.bonificacion_permanente_pagada_urls')),      


    # funciones
    path('trabajador/nombre/<int:cedula>/', trabajador.trabajador_nombre_apellido ),
    path('concepto/objeto/<int:obj>/', concepto.filtrar_conceptos_objeto, name='filtrar_conceptos_objeto'),    
    path('concepto/nomina/filtrar/', concepto.concepto_nomina_filtrar, name='filtrar_concepto_nomina'),
    path('cargo/filtrar/', cargo.cargo_filtrar, name='filtrar_cargo'), 
    path('mins/results/', mins.mins_results, name='mins_results'), 

    path('periodo/filtrar/', periodo.periodo_filtrar, name='periodo_filtrar'), 
    path('periodo/id/', periodo.get_periodo_by_id, name='periodo_id'), 



    path('procesos/', include('app.urls.proceso_urls') ), 

    path('tipo_cargo/filtrar/<int:id>/', tipo_cargo.tipo_cargo_filtrar, name='filtrar_tipo_cargo'),



    # error
    path('error_permisos/', error_permisos, name='error_permisos'), 



    # prueba
    path('crear_socio/', SocioCrearView.as_view(), name='crear_socio'),

] 




'''

   
path('mins/', include('core.institucion.urls')),   
path('periodo/', include('core.periodo.urls')),         



# tablas maestras   
path('trabajador/nombre/<int:cedula>/', trabajador.trabajador_nombre_apellido ),

path('concepto/objeto/<int:obj>/', concepto.filtrar_conceptos_objeto, name='filtrar_conceptos_objeto'),

path('cargo/filtrar/', cargo.cargo_filtrar, name='filtrar_cargo'), 

path('concepto_nomina/filtrar/', concepto.concepto_nomina_filtrar, name='filtrar_concepto_nomina'),
'''